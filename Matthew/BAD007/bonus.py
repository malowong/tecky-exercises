from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.neighbors import KNeighborsClassifier

iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)

iris_knn_classifier = KNeighborsClassifier(n_neighbors=3)
iris_knn_classifier.fit(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))