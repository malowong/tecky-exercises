from dataclasses import dataclass, field

# compare=false: even if age is different, we cannot be used to compare whether they are the same or not
# defalut=0.0: default
# order=True: the data can be ordered
# init=False: the data can be initialized without value
# repr=False: the sort index will not be printed when we printed the instance(object)

# why there are two __sort_index? because we want to sort them by cash if some of the data have the same age (cannot be sorted by age)


@dataclass(order=True)
class Person:
    __sort_index_age: float = field(init=False, repr=False)
    __sort_index_cash: float = field(init=False, repr=False)
    name: str
    age: int = field(compare=False)
    cash: float = field(default=0.0, compare=False)

    def __post_init__(self):
        self.__sort_index_cash = self.cash
        self.__sort_index_age = self.age


dicky = Person(name='Dicky', age=10, cash=10)
jason = Person(name='Jason', age=10, cash=10)
alex = Person(name='Alex', age=50, cash=100)

lis = [alex, jason, dicky]
lis.sort()
print(lis)