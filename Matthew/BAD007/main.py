import numpy as np
import math
from dataclasses import dataclass, field
from scipy.stats  import mode
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

@dataclass(order=True)
class KNNClassifier:

    # __sort_index is used to sort the dist_list by the X_train data.
    # if we want to sort some complex data type (dist_list in this case, which is a array of X_train and y_train), we can use dataclass.

    def __init__(self, X_train, y_train, n_neighbors = 3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train
        __sort_index = field(init=False, repr=False)

    def __post_init__(self):
        self.__sort_index = self.X_train

    """calculate the euclidean distance here"""
    def euclidean_dist(self, point_1, point_2):
        if len(point_1) != len(point_2):
          print("Invalid Dimension Length")
          return
        sum = 0
        for i in range(len(point_1)):
          diff = point_1[i] - point_2[i]
          val = diff * diff
          sum += val
        return math.sqrt(sum)

    """accept multiple inputs here and predict one by one with predict_single()"""
    def predict(self, X_test_array):
        predict_list = []

        for i in X_test_array:
            predict_list.append(self.predict_single(i))
        return predict_list


    """predict single input here"""
    def predict_single(self, input_data_single):
        dist_list = []
        neighbors_list = []

        for i in range(len(self._X_train)):
            dist = [self.euclidean_dist(input_data_single, self._X_train[i]), self._y_train[i]]
            dist_list.append(dist)
            dist_list.sort()
            if len(dist_list) >= 4:
                dist_list.pop()
        
        print(dist_list)

        for i in range(self.n_neighbors):
            neighbors_list.append(dist_list[i][1])
        
        return max(set(neighbors_list), key = neighbors_list.count)

iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)

iris_knn_classifier = KNNClassifier(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))