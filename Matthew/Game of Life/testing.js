const unitLength = 20
const boxColor = 150
const strokeColor = 50
let columns /* To be determined by window width */
let rows /* To be determined by window height */
let currentBoard
let nextBoard
let canvas

function setup() {
  /* Set the canvas to be under the element #canvas*/
  canvas = createCanvas(windowWidth, windowHeight - 100)
  canvas.parent(document.querySelector('#canvas'))

  /*Calculate the number of columns and rows */
  columns = floor(width / unitLength)
  rows = floor(height / unitLength)

  /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
  currentBoard = []
  nextBoard = []
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = []
    nextBoard[i] = []
  }
  // Now both currentBoard and nextBoard are array of array of undefined values.
  init() // Set the initial values of the currentBoard and nextBoard
}

/**
 * Initialize/reset the board state
 */
function init() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = 0
      nextBoard[i][j] = 0
    }
  }
}

function draw() {
    console.log("hey")
  
  background(255)
  generate()
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (currentBoard[i][j] == 1) {
        fill(boxColor)
      } else {
        fill(255)
      }
      stroke(strokeColor)
      rect(i * unitLength, j * unitLength, unitLength, unitLength)
    }
  }
}

function generate() {
  //Loop over every single box on the board
  for (let x = 0; x < columns; x++) {
    for (let y = 0; y < rows; y++) {
      // Count all living members in the Moore neighborhood(8 boxes surrounding)
      let neighbors = 0
      for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
          if (i == 0 && j == 0) {
            // the cell itself is not its own neighbor
            continue
          }
          // The modulo operator is crucial for wrapping on the edge
          neighbors +=
            currentBoard[(x + i + columns) % columns][(y + j + rows) % rows]
        }
      }

      // Rules of Life
      if (currentBoard[x][y] == 1 && neighbors < 2) {
        // Die of Loneliness
        nextBoard[x][y] = 0
      } else if (currentBoard[x][y] == 1 && neighbors > 3) {
        // Die of Overpopulation
        nextBoard[x][y] = 0
      } else if (currentBoard[x][y] == 0 && neighbors == 3) {
        // New life due to Reproduction
        nextBoard[x][y] = 1
      } else {
        // Stasis
        nextBoard[x][y] = currentBoard[x][y]
      }
    }
  }

  // Swap the nextBoard to be the current Board
  ;[currentBoard, nextBoard] = [nextBoard, currentBoard]
}

/**
 * When mouse is dragged
 */
function mouseDragged() {
  if (event.target != canvas.canvas) {
    // console.log('not canvas')
    return
  }
//   console.log(event)
  /**
   * If the mouse coordinate is outside the board
   */
  if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
    return
  }
  const baseX = Math.floor(mouseX / unitLength)
  const baseY = Math.floor(mouseY / unitLength)

  // this can be used for eraser too (if the pattern is full of '.')
  let patternText = `
...o...
..ooo..
...o...
`
  let lines = patternText.split('\n').filter(line => line.length > 0)
  for (let patternY = 0; patternY < lines.length; patternY++) {
    let line = lines[patternY]
    for (let patternX = 0; patternX < line.length; patternX++) {
      let char = line[patternX]

      let y = baseY + patternY
      let x = baseX + patternX

      if (char == '.') {
        currentBoard[x][y] = 0
        fill(255)
      } else {
        currentBoard[x][y] = 1
        fill(boxColor)
      }
      stroke(strokeColor)
      rect(x * unitLength, y * unitLength, unitLength, unitLength)
    }
  }
}

/**
 * When mouse is pressed
 */
function mousePressed() {
  noLoop()
  mouseDragged()
}

/**
 * When mouse is released
 */
function mouseReleased() {
  loop()
}

document.querySelector('#reset-game').addEventListener('click', function () {
  init()
})

let patternText = `
!Name: Gosper glider gun
!
........................O...........
......................O.O...........
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO..............
OO........O...O.OO....O.O...........
..........O.....O.......O...........
...........O...O....................
............OO......................
`

function placePattern() {
  // currentBoard[5][5] = 1
  // currentBoard[5][6] = 1
  // currentBoard[6][5] = 1
  // currentBoard[6][6] = 1

  let lines = patternText.split('\n')
  lines = lines.filter(line => line.length > 0)
  lines = lines.filter(line => !line.startsWith('!'))

  for (let y = 0; y < lines.length; y++) {
    let line = lines[y]
    for (let x = 0; x < line.length; x++) {
      let char = line[x]
      if (char == '.') {
        currentBoard[x][y] = 0
      } else {
        currentBoard[x][y] = 1
      }
    }
  }
}

document.querySelector('#draw-pattern').addEventListener('click', placePattern)
