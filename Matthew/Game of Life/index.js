let unitLength = 10;
const boxColor = 'red';
const strokeColor = "darkgrey";
let columns; // To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;

let speedSlider;
let sizeSlider;

let x;
let y;

let countScore = -1;
let countHTML

let countloop = 0;

function setup() {
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth - 50, windowHeight - 100);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /* Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.

    // speedSlider

    speedSlider = createSlider(1, 9, 6, 1);
    speedSlider.position(70, 50, 'fixed');
    speedSlider.style('width', '80px');


    // sizeSlider

    sizeSlider = createSlider(1, 9, 1, 1);
    sizeSlider.position(190, 50, 'fixed')
    sizeSlider.style('width', '80px')


    init();  // Set the initial values of the currentBoard and nextBoard
}

/* Initialize/reset the board state */

function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
    noLoop()
}

function draw() {
    background(34, 37, 41);

    // expontential speedSlider

    let speedVal = speedSlider.value()
    if (speedVal > 5) {
        speedVal = 5 * pow(2.1, speedVal - 5)
    }

    frameRate(speedVal);

    // sizeSlider

    let sizeVal = sizeSlider.value()

    if (sizeVal < 5) {
        sizeVal = 12 + (sizeVal - 1) * 2
    } else if (sizeVal > 5) {
        sizeVal = 20 + (sizeVal - 5) * 10
    } else {
        sizeVal = 20
    }

    unitLength = sizeVal

    if (sizeSlider.value() <= 5) {
        resizeCanvas(floor(windowWidth / unitLength) * unitLength - unitLength * (9 - sizeSlider.value()), floor(windowHeight / unitLength) * unitLength - unitLength * (14 - sizeSlider.value()))
    } else {
        resizeCanvas(floor(windowWidth / unitLength) * unitLength - unitLength, floor(windowHeight / unitLength) * unitLength - unitLength * (11 - sizeSlider.value()))
    }

    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(106, 235, 233);
                if (countloop >= 100 && countloop < 200) {
                    fill(121, 249, 83)
                } else if (countloop >= 200 && countloop < 300) {
                    fill(234, 65, 247)
                } else if (countloop >= 300 && countloop < 400) {
                    fill(255, 253, 84)
                } else if (countloop >= 400) {
                    fill(235, 50, 35)
                }
            } else if (currentBoard[i][j] == 0) {
                fill(34, 37, 41);
            }
            noStroke();
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }

    countScore += 1;
    countHTML = `<h5>${countScore}</h5>`
    document.querySelector('.score2').innerHTML = countHTML

    countloop += 1;

    if (countloop == 500) {
        countloop = 0;
    }

}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}


/* When mouse is dragged */
function mouseDragged() {
    /* If the mouse coordinate is outside the board */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(106, 235, 233);
    noStroke();
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/* When mouse is pressed */
function mousePressed() {

    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }

    x = Math.floor(mouseX / unitLength);
    y = Math.floor(mouseY / unitLength);

    if (currentBoard[x][y] == 0) {
        currentBoard[x][y] = 1;
        fill(106, 235, 233);
        noStroke();
        rect(x * unitLength, y * unitLength, unitLength, unitLength);
    } else {
        currentBoard[x][y] = 0;
        fill(34, 37, 41);
        // stroke('white');
        rect(x * unitLength, y * unitLength, unitLength, unitLength);
    }
}

/* When mouse is released */

// function mouseReleased() {
//     loop();
// }

// function fillDifferentColor() {
//     if () {
//         return fill("blue")
//     }

// }

let reset = document.querySelector('.reset-game')
let start = document.querySelector('.start')

reset.addEventListener('click', function () {
    countScore = -2;
    countloop = -2;
    init();
    draw();
    if (start.innerHTML == 'STOP') {
        start.innerHTML = 'START'
    }
    document.querySelector('#canvas').removeEventListener('click', clicker)
})

start.addEventListener('click', function (event) {
    if (event.target.innerHTML == 'START') {
        loop();
        event.target.innerHTML = 'STOP'
    } else if (event.target.innerHTML == 'STOP') {
        noLoop();
        event.target.innerHTML = 'START'
    }
    document.querySelector('#canvas').removeEventListener('click', clicker)
})

let clicker;

document.querySelector('.glider').addEventListener('click', function (event) {
    document.querySelector('#canvas').removeEventListener('click', clicker)

    let clickX;
    let clickY;

    clicker = function () {
        clickX = Math.floor(mouseX / unitLength);
        clickY = Math.floor(mouseY / unitLength);
        drawPattern(gliderText, clickX, clickY);
        redraw();
    }

    document.querySelector('#canvas').addEventListener('click', clicker)

})

document.querySelector('.lwss').addEventListener('click', function () {

    document.querySelector('#canvas').removeEventListener('click', clicker)

    let clickX;
    let clickY;

    clicker = function () {
        clickX = Math.floor(mouseX / unitLength);
        clickY = Math.floor(mouseY / unitLength);
        drawPattern(lwssText, clickX, clickY);
        redraw();
    }
    document.querySelector('#canvas').addEventListener('click', clicker)
})

document.querySelector('.mwss').addEventListener('click', function () {

    document.querySelector('#canvas').removeEventListener('click', clicker)

    let clickX;
    let clickY;

    clicker = function () {
        clickX = Math.floor(mouseX / unitLength);
        clickY = Math.floor(mouseY / unitLength);
        drawPattern(mwssText, clickX, clickY);
        redraw();
    }
    document.querySelector('#canvas').addEventListener('click', clicker)
})

document.querySelector('.hwss').addEventListener('click', function () {

    document.querySelector('#canvas').removeEventListener('click', clicker)

    let clickX;
    let clickY;

    clicker = function () {
        clickX = Math.floor(mouseX / unitLength);
        clickY = Math.floor(mouseY / unitLength);
        drawPattern(hwssText, clickX, clickY);
        redraw();
    }
    document.querySelector('#canvas').addEventListener('click', clicker)
})

document.querySelector('.gun').addEventListener('click', function () {

    document.querySelector('#canvas').removeEventListener('click', clicker)

    let clickX;
    let clickY;

    clicker = function () {
        clickX = Math.floor(mouseX / unitLength);
        clickY = Math.floor(mouseY / unitLength);
        drawPattern(gunText, clickX, clickY);
        redraw();
    }
    document.querySelector('#canvas').addEventListener('click', clicker)
})


// function clickX() {
//     document.body.addEventListener('click', function() {

//         if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
//             return;
//         }

//         const x = Math.floor(mouseX / unitLength);
//         return console.log(parseInt(x))
//     })
// }

// function clickY() {
//     document.body.addEventListener('click', function() {

//         if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
//             return;
//         }

//         const y = Math.floor(mouseY / unitLength);
//         return console.log(parseInt(y))
//     })
// }


// window.addEventListener('keypress', function(event) {
//     if (event.key == "r") {
//         console.log(rotatePattern(testText))
//     }
// })

function windowResized() {
    resizeCanvas(floor(windowWidth / unitLength) * (unitLength - 1),);
}

function resize() {
    loop()
    translate(width / 2, height / 2)
    size(2)
}

let gliderText = `
.O.
..O
OOO
`

let lwssText = `
.*..*
*....
*...*
****.
`

let mwssText = `
...*..
.*...*
*.....
*....*
*****.
`

let hwssText = `
...**..
.*....*
*......
*.....*
******.
`

let gunText = `
........................O...........
......................O.O...........
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO..............
OO........O...O.OO....O.O...........
..........O.....O.......O...........
...........O...O....................
............OO......................
`

function rotatePattern(text) {
    let lines = text.split('\n')
    lines = lines.filter(function(lines) {
        return lines.length > 0
    })
    let newLines



    for (let i = 0; i < lines.length; i++){
        let line = lines[i]

        for (let j = 0; j < 5; j++){

            // let char = line[j]
            console.log(line)

            // newLines[lines.length - 1 - j][]


            // // if (char ???) {
            // //     newLines[lines.length - 1] = char
            // // }
            newLines += lines[lines.length - 1 - j][i]

        }
        newLines += '\n'
    }
    return newLines
}



function drawPattern(text, x, y) {

    let lines = text.split('\n')
    lines = lines.filter(function (lines) {
        return lines.length > 0
    })
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i]
        for (let j = 0; j < line.length; j++) {
            let char = line[j]

            // if (char == ".") {
            //     currentBoard[j][i] = 0
            // } else if (char == "O" || char == "*") {
            //     currentBoard[j][i] = 1
            // }

            // let x = parseInt(window.prompt("where do you want to place your pattern? (USAGE: length, width)"))
            // let y = parseInt(window.prompt("where do you want to place your pattern? (USAGE: length, width)"))

            // let x = parseInt(a[0])
            // let y = parseInt(a[1])

            if (char == ".") {
                currentBoard[(j + x)][(i + y)] = 0
            } else if (char == "O" || char == "*") {
                currentBoard[(j + x)][(i + y)] = 1
            }
        }
    }

}

// arcade game style (background, color of annihilating pattern, font style)
// every addeventlistener different color, 

// done: 
// 1. start-stop button
// 2. speed slider
// 3. press to delete
// 4. selecting patterns
// 5. responsive
// 6. 

// question: 
// 1. how to dragged pattern and insert pattern without redrawing? 
// from benno's example (WEF008) we can see that mouseDragged function could help to perform the "draw under noLoop" miracle.
// but under current situation it is hard to use addEventListener together with mouseDragged. 

// 2. how to activate slider when it is not yet drawed? 
// as suggested above, you can put it in the mousedragged function as well. but it requires a recalculation of the 
// size. (resizeCanvas)

// 3. how to rotate pattern? 
// 

// 4. how to make different pattern in different colors: store color data in every box, currentboard 0 = nolive; have number = color
