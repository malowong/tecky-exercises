class Player {
   strength: number;
   name: string;
   exp: number;

   constructor(strength: number, name: string) {
      this.strength = strength;
      this.name = name;
      this.exp = 0;
      console.log(`Name: ${this.name}`)
   }

   gainExperience(exp: number, monster: Monster) {
      this.exp += exp
      console.log(`${this.name} has gained ${exp} exp! Total: ${this.exp} exp`)
  }
}

class Amazon extends Player {
   primary: string;
   secondary: string;
   usePrimaryAttack: "Primary" | "Secondary"

   constructor(name: string, usePrimaryAttack: "Primary" | "Secondary") {
      let strength:number = 0
      strength = usePrimaryAttack == "Primary" ? 30 : 40
      super(strength, name);
      this.primary = "Bow and Arrow";
      this.secondary = "Throwing Spear";
      this.usePrimaryAttack = usePrimaryAttack
      console.log(`Player Type: ${this.constructor.name}; Attack Style: ${this.usePrimaryAttack}`)
   }

   attack(monster: Monster){
      let exp = monster.hp
      while (monster.hp > 0){
         monster.injure(this.strength)
         if(this.usePrimaryAttack == "Primary"){
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.primary})`)
         } else {
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.secondary})`)
         }
      }
      console.log(`${this.name} wins!`)
      this.gainExperience(exp, monster)
   }

   switchAttack() {
      this.usePrimaryAttack = this.usePrimaryAttack == "Primary" ? "Secondary" : "Primary"
      this.strength = this.usePrimaryAttack == "Primary" ? 30 : 40
      console.log(`The attack style of ${this.name} has been changed to ${this.usePrimaryAttack}!`)
   }
}

class Paladin extends Player {
   primary: string;
   secondary: string;
   usePrimaryAttack: "Primary" | "Secondary"

   constructor(name: string, usePrimaryAttack: "Primary" | "Secondary") {
      let strength:number = 0
      strength = usePrimaryAttack == "Primary" ? 50 : 25
      super(strength, name);
      this.primary = "Swords";
      this.secondary = "Magic Spells";
      this.usePrimaryAttack = usePrimaryAttack
      console.log(`Player Type: ${this.constructor.name}; Attack Style: ${this.usePrimaryAttack}`)
   }

   attack(monster: Monster){
      let exp = monster.hp
      while (monster.hp > 0){
         monster.injure(this.strength)
         if(this.usePrimaryAttack == "Primary"){
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.primary})`)
         } else {
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.secondary})`)
         }
      }
      console.log(`${this.name} wins!`)
      this.gainExperience(exp, monster)
   }

   switchAttack() {
      this.usePrimaryAttack = this.usePrimaryAttack == "Primary" ? "Secondary" : "Primary"
      this.strength = this.usePrimaryAttack == "Primary" ? 50 : 25
      console.log(`The attack style of ${this.name} has been changed to ${this.usePrimaryAttack}!`)
   }
}

class Barbarian extends Player {
   primary: string;
   secondary: string;
   usePrimaryAttack: "Primary" | "Secondary"

   constructor(name: string, usePrimaryAttack: "Primary" | "Secondary") {
      let strength:number = 0
      strength = usePrimaryAttack == "Primary" ? 55 : 30
      super(strength, name);
      this.primary = "Swords";
      this.secondary = "Throwing Spear";
      this.usePrimaryAttack = usePrimaryAttack
      console.log(`Player Type: ${this.constructor.name}; Attack Style: ${this.usePrimaryAttack}`)
   }

   attack(monster: Monster){
      let exp = monster.hp
      while (monster.hp > 0){
         monster.injure(this.strength)
         if(this.usePrimaryAttack == "Primary"){
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.primary})`)
         } else {
            console.log(`${this.name} attacks a monster! (HP: ${monster.hp} with ${this.secondary})`)
         }
      }
      console.log(`${this.name} wins!`)
      this.gainExperience(exp, monster)
   }

   switchAttack() {
      this.usePrimaryAttack = this.usePrimaryAttack == "Primary" ? "Secondary" : "Primary"
      this.strength = this.usePrimaryAttack == "Primary" ? 55 : 30
      console.log(`The attack style of ${this.name} has been changed to ${this.usePrimaryAttack}!`)
   }
}

class Monster {
   hp: number;
   constructor() {
       this.hp = 100;
   }
   // Think of how to write injure
   injure(strength: number){
       if (this.hp < strength){
           this.hp = 0
       } else {
           this.hp -= strength
       }
   }
}

const player1 = new Amazon("Ben", "Primary")
const player2 = new Paladin("Amy", "Secondary")
const player3 = new Barbarian("Peter", "Primary")

const monster1 = new Monster();
const monster2 = new Monster();
const monster3 = new Monster();
const monster4 = new Monster();
const monster5 = new Monster();
const monster6 = new Monster();

player1.attack(monster1);
player1.switchAttack();
player1.attack(monster2);

player2.attack(monster3);
player2.switchAttack();
player2.attack(monster4);

player3.attack(monster5);
player3.switchAttack();
player3.attack(monster6);