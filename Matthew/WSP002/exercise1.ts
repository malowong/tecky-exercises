// Declaration of Class and its methods
export class Player {
    private strength: number;
    private name: string;
    private exp: number;
    constructor(strength: number, name: string) {
        this.strength = strength;
        this.name = name;
        this.exp = 0;
        console.log(`Player Name: ${this.name} Player Strength: ${this.strength}`)
    }

    attack(monster: Monster) {
        let exp = monster.hp
        while (monster.hp > 0){
            if (Math.random() < 0.25){
                monster.injure(this.strength * 2)
                console.log(`${this.name} attacks a monster (HP: ${monster.hp}) [critical]`)
            } else {
                monster.injure(this.strength)
                console.log(`${this.name} attacks a monster (HP: ${monster.hp})`)
            }
        }
        this.gainExperience(exp)
    }

    gainExperience(exp: number) {
        this.exp += exp
        console.log(`${this.name} has gained ${this.exp} exp!`)
    }
}


export class Monster {
    hp: number;
    constructor() {
        this.hp = 100;
    }
    // Think of how to write injure
    injure(strength: number){
        if (this.hp < strength){
            this.hp = 0
        } else {
            this.hp -= strength
        }
    }
}


// Invocations of the class and its methods
const player = new Player(20, 'Peter');
const monster = new Monster();
player.attack(monster);

const player2 = new Player(30, 'Alice');
const monster2 = new Monster();
player2.attack(monster2);


// English counterpart: Player attacks monster