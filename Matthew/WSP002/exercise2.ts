export class Style{
    playerType: string;
    isPrimary: boolean;
    damage:number

    constructor(playerType: string, isPrimary: boolean){
        this.playerType = playerType
        this.isPrimary = isPrimary
        this.damage = 0
    }

    checkWeapon(){
        if (this.playerType == "Amazon" && this.isPrimary == true){
            this.damage = 30
        }else if (this.playerType == "Amazon" && this.isPrimary == false){
            this.damage = 40
        }
        if (this.playerType == "Paladin" && this.isPrimary == true){
            this.damage = 50
        }else if (this.playerType == "Paladin" && this.isPrimary == false){
            this.damage = 25
        }
        if (this.playerType == "Barbarian" && this.isPrimary == true){
            this.damage = 55
        }else if (this.playerType == "Barbarian" && this.isPrimary == false){
            this.damage = 30
        }
    }
}

// interface Player{
//     attack(monster:Monster): void;
//     switchAttack(): void;
//     gainExperience(exp:number): void;
// }

class Player{
    private attackStyle: string;
    private playerType: string;
    private attack: Style;

    private name: string;
    private strength: number;
    private exp: number;

    constructor(name: string, playerType: string, attackStyle: string){
        if (this.attackStyle == "Primary"){
            this.attack = new Style(this.playerType, true)
        } else {
            this.attack = new Style(this.playerType, false)
        }
        console.log(this.attack)
    }

    attackToDie(monster: Monster) {
        let exp = monster.hp
        while (monster.hp > 0){
            if (Math.random() < 0.25){
                monster.injure(this.strength * 2)
                console.log(`${this.name} attacks a monster (HP: ${monster.hp}) [critical]`)
            } else {
                monster.injure(this.strength)
                console.log(`${this.name} attacks a monster (HP: ${monster.hp})`)
            }
        }
        this.gainExperience(exp)
    }

    gainExperience(exp: number) {
        this.exp += exp
        console.log(`${this.name} has gained ${this.exp} exp!`)
    }

    switchAttack(){
        if (this.attackStyle == "Primary"){
            this.attack = new Style(this.playerType, false)
        } else {
            this.attack = new Style(this.playerType, true)
        }
    }
}

class Monster {
    hp: number;
    constructor() {
        this.hp = 100;
    }
    // Think of how to write injure
    injure(strength: number){
        if (this.hp < strength){
            this.hp = 0
        } else {
            this.hp -= strength
        }
    }
}


// class Amazon implements Player{
//     private primary: Attack;
//     private secondary: Attack;
//     private usePrimaryAttack: boolean;

//     private name: string;
//     private strength: number;
//     private exp: number;

//     constructor(strength: number, name: string, usePrimaryAttack: boolean){
//         this.primary = new BowAndArrow(30);
//         this.secondary = new ThrowingSpear(40);
//         // TODO: set the default value of usePrimaryAttack
//         this.usePrimaryAttack = usePrimaryAttack;
//         this.name = name;
//         this.strength = strength;
//         this.exp = 0;
//         this.hit = 0;
//         console.log(`Player Name: ${this.name} Player Strength: ${this.strength} usePrimaryAttack: ${this.usePrimaryAttack}`)
//      }

//      attack(monster:Monster):void{
//          if(this.usePrimaryAttack){
//             if (monster.hp > 0){
//                 this.hit++
//                 monster.injure(this.name, this.strength, this, , this.secondary)
//             }
//             // TODO: use primary attack
//          }else{
//             if (monster.hp > 0){
//                 this.hit++
//                 monster.injure(this.name, this.strength, this)
//             }
//             // TODO: use secondary attack
//          }
//      }

//      switchAttack(){
//         this.usePrimaryAttack = false
//          // TODO: Change the attack mode for this player

//      }

//      //.. The remaining methods.

//      gainExperience() {
//         this.exp += this.hit * 10
//         console.log(`${this.name} has gained ${this.exp} exp!`)
//     }
// }

// class Monster{
//     hp: number;
//     constructor() {
//         this.hp = 100;
//     }

//     injure(name: string, strength: number, player: Amazon, primary: ){
//         let critical = Math.floor(Math.random() * 5)
//         if (critical == 0){
//             this.hp -= strength * 2
//             console.log(`${name} attacks a monster (HP: ${this.hp}) [critical]`)
//         } else {
//             this.hp -= strength
//             console.log(`${name} attacks a monster (HP: ${this.hp})`)
//         }
//         if (this.hp <= 0){
//             player.gainExperience()
//         }
//     }
// }

const player = new Player("Peter", "Amazon", "Primary");
const monster = new Monster();
player.attackToDie(monster);

player.switchAttack();
const monster2 = new Monster();
player.attackToDie(monster2);
