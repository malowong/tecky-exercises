export function fizzbuzz(number: number){

    let output: string | undefined = ''

    if (number == 0) {
        output = undefined;
        return
    }

    for (let i = 1; i <= number; i++){

        if (i % 15 == 0){
            output += ', Fizz Buzz'
        } else if (i % 3 == 0){
            output += ', Fizz'
        } else if (i % 5 == 0){
            output += ', Buzz'
        } else if (i > 1){
            output +=  `, ${i}`
        } else if (i == 1){
            output = "1"
        } 
    }
    console.log(output)

    return output
}

fizzbuzz(100)