import { fizzbuzz } from './fizzbuzz'

test('fizzbuzz test', () => {

    expect(fizzbuzz(0)).toBeUndefined()

    expect(fizzbuzz(3)).toEqual("1, 2, Fizz")

    expect(fizzbuzz(5)).toEqual("1, 2, Fizz, 4, Buzz")

    expect(fizzbuzz(10)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz")

    expect(fizzbuzz(12)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz")

    expect(fizzbuzz(15)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz")
})