import {factorial, fibonacci} from "./factorial_fibonacci"

describe("test factorial", () => {

    // one test case for one logic, a function can have multiple test case

    it("test factorial with negative", () => {
        // we have to use function to wrap our function (toThrow)
        const fn = () => factorial(-1)
        expect(fn).toThrow("not support negative numbers")
    })

    it("test factorial with constant", () => {
        expect(factorial(5)).toBe(120);
    })

})

describe("test fibonacci", () => {

    test('fibonacci test', () => {
        expect(fibonacci(1)).toBe(1);
        expect(fibonacci(2)).toBe(1);
        expect(fibonacci(3)).toBe(2);
        expect(fibonacci(4)).toBe(3);
        expect(fibonacci(5)).toBe(5);
    
    });
    
})

