import {phi} from 'mathjs'

export function factorial(num:number):number{
    if (num < 0){
        throw new Error ("not support negative numbers")
    }

    if(num == 0 || num == 1){
        return 1;
     }
    return factorial(num -1 )* num
}

export function fibonacci(num:number){

    if (num < 0){
        throw new Error ("not support negative numbers")
    }

    let first = 0
    let second = 1
    let next = null

    for (let i = 0; i <= num; i++) {
        if (i <= 1){
            next = i
        } else {
           next = first + second;
           first = second;
           second = next;
        }
    }

    return next

    // original answer from cms
    // if(num == 1 || num == 2){
    //     return 1;
    //  }
    //  return fibonacci(num-1)+ fibonacci(num -2)
}

console.log(fibonacci(5))


export function fibonacciGolden(num:number):number{
    let ans = Math.floor((phi**num - (1 - phi)**num) / Math.sqrt(5))
    return ans
}

// console.log(fibonacciGolden(70))