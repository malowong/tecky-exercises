import React from 'react';
import './App.css';
import './App.scss';
import Board from './components/Board';


const element = <h1>Tic-tac-toe</h1>


function App() {
  return (
    <div className="App">
      {element}
      <Board />
    </div>
  );
}

export default App;



