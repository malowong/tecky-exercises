import React, { useState } from 'react';
import Square from './Square';
import styles from '../css/Board.module.css'

export enum squareValues {
    Cross = "X",
    Circle = "O",
    Null = "",
}

export default function Board(){
    const [squares, setSquares] = useState(Array(9).fill(squareValues.Null))
    const [round, setRound] = useState(1)

    // moving state up
    // that's why clickHandler is put here but not Squares component.
    const clickHandler = (pos: number) => {
        // immutability
        const newSquares = squares.slice()
        const target = pos % 2 === 0 ? squareValues.Cross : squareValues.Circle
        newSquares[pos] = target
        setSquares(newSquares)
        setRound(round + 1)
    }

    const restartHandler = () => {
        const newSquares = squares.slice()
        setSquares(Array(9).fill(squareValues.Null))
        setRound(1)
    }

    return (
        // we have to set a dummy div here because it only accepts one div.
        <>
            <h1>Round: {round}</h1>
            <div className={styles.board}>
                {squares.map((squareValue, idx) => (
                    <Square key={idx} value={squareValue} updateSquare={() => {
                        clickHandler(idx)
                    }}/>
                ))}
            </div>
            <button onClick={restartHandler}><h1>Restart</h1></button>
        </>
    )
} 