import React from 'react';
import styles from '../css/Square.module.css'

interface IProps{
    key: number,
    value: string,
    updateSquare: () => void,
}


export default function Square(props: IProps) {
  return (
    <div className={styles.square} onClick={props.updateSquare}>{props.value}</div>
  );
}
