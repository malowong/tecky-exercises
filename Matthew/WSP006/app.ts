import express from "express";
import path from "path";
import expressSession from 'express-session';

const app = express();

// middleware in the form of app.use

app.use((req, res, next) => {
    console.log(`[${new Date().toISOString()}] Request ${req.path}`)
    next()
})

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

// route handler: (HTML methods + path pattern)

// app.get() -> handle request with HTTP method = GET
// "/" -> handle request with path pattern = "/"
// HTTP method = GET && path pattern = "/"

// req -> express.Request
// res -> express.Response

app.get("/index.html",
    // another middleware inside app.get
    (req, res, next) => {

        if (req.session['counter'] >= 0){
            req.session['counter'] += 1
        } else if (!req.session['counter']){
            req.session['counter'] = 1
        }
        console.log(req.session);
        next()
})

// another middleware that is used to serve static files. by that we mean to serve all public folder (asset)

app.use(express.static(path.join(__dirname, "assets")))

app.use((req,res)=>{
    res.sendFile(path.resolve("./assets/404.html"));
});

// path.resolve is similar to path.join, but resolve can help you to turn the address to absolute

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`[info] listening to port ${PORT}`);
})