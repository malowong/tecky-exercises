// encoding type of /memo: multipart/form-data	
// encoding type of /login: application/x-www-form-urlencoded

// the use of app testing tools (i.e. insomnia) is to test the back-end envioronment. 
// in this case, it can not help to redirect to other html files. 
// so use insomnia first for testing if back-end is working well, then browser for overall checking.

// two error: one is missing out the ending tag </form>, one is mixing up = with == ...

import express from "express";
import path from "path";
import expressSession from 'express-session';
import jsonfile from "jsonfile";
import multer from "multer";

// define type

type Memo = {
    content: string;
    filename?: string;
};

type Users = {
    username: string;
    password: string;
}

// using express, make sure express handle the json file, make sure express handle the form data

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// multer

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
const upload = multer({ storage });

// browser showing memo data (localhost:8080/memo)

app.get("/memo", async (req, res) => {
    const memo: Memo[] = await jsonfile.readFile(path.join(__dirname, "memo.json"));
    res.json({ memo });
});

// update memo

app.post("/memo", upload.single("image"), async (req, res) => {
    console.log(req.file);
    console.log(req.body);

    // read data from jsonfile
    const memo: Memo[] = await jsonfile.readFile(path.join(__dirname, "memo.json"));

    // add a new student record to the array
    memo.push({
        content: req.body.content,
        filename: req.file?.filename,
    });

    // save the data into jsonfile
    await jsonfile.writeFile(path.join(__dirname, "memo.json"), memo);

    // redirect the client to new location
    res.redirect("/complete.html");

});

// login

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.post("/login", async(req, res) => {

    const login: Users[] = await jsonfile.readFile(path.join(__dirname, "users.json"))

    login.push({
        username: req.body.username,
        password: req.body.password,
    })

    await jsonfile.writeFile(path.join(__dirname, "users.json"), login)

    if(req.body.username == "123" && req.body.password == "123"){
        req.session['user'] = true
        console.log("login success")
        res.redirect("/admin.html")
        return
    }
    console.log("login failed")
    res.redirect("/incorrect.html")
})

// seemingly there are two login validation here. one above and one below.
// the one above is used to check at the moment you press the login button. 
// the one below is used to check whenever you are backed to the website, i.e. anytime. 
// that's why you have to redirect to index.html in the else condition below. 
// coz when a guy who is not admin access to your page, he is supposed to visit the index.html instead of incorrect.html. 
// only when you press login but incorrect will you get access to incorrect.html.

const isLoggedIn = (req:express.Request,res:express.Response,next:express.NextFunction)=>{

    if(req.session && req.session['user']){
        //called Next here
        next()

    }else{
       // redirect to index page
       res.redirect("/index.html")
    }
}

// this is for debugging

app.use((req, res, next) => {
    const time = new Date().toISOString()
    console.log(`[INFO] ${time} req path: ${req.path} req method: ${req.method}`)
    next()
})

// static file & 404
app.use(express.static(path.join(__dirname, "public")))

// admin.html should be inside protected
app.use(isLoggedIn,express.static('protected'));


app.use((req,res)=>{
    res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`[info] listening to port ${PORT}`);
})