import fs from "fs"
import Path from "path"
import util from "util"
import readline from 'readline';

export function questionPromise(query: string) {
    return new Promise<string>(function (resolve) {
        const readLineInterface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        readLineInterface.question(query, (answer) => {
            resolve(answer)
            readLineInterface.close();
        })
    })
}

export async function listAllJsRecursive(path: string) {
    try {
        let fsReaddirPromise = util.promisify(fs.readdir)
        let files = await fsReaddirPromise(path);

        for (let file of files) {
            // if you prefer to use util.promisify:
            // beware that not all of the callback function are able to use this method
            let fsStatPromise = util.promisify(fs.stat)
            let stat = await fsStatPromise(Path.join(path, file));

            if (stat.isDirectory() != true && file.substring(file.length - 3) == ".js") {
                // base case
                Path.join(path, file)
            } else if (stat.isDirectory() == true) {
                // recursive case
                // ATTENTION: need await! 
                await listAllJsRecursive(Path.join(path, file))
            }
        }
    } catch (err) {
        console.error(err);
    }
}