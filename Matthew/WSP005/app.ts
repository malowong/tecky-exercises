import { questionPromise } from './export';
import { listAllJsRecursive } from './export';

import os from "os"
import fs from "fs"

const readCommand = async () => {
    while (true) { // game-loop, eval-loop
        // Exit by Ctrl+C
        const answer = await questionPromise("Please choose read the report(1) or run the benchmark(2):");
        const option = parseInt(answer, 10);
        console.log(`Option ${answer} chosen.`);
        if (option == 1) {
            await readTheReport();
        } else if (option == 2) {
            await runTheBenchmark();
        } else {
            console.log("Please input 1 or 2 only.");
        }
    }
}

async function runTheBenchmark() {
    let report: Trial[] = []

    // 1 time
    await runProgram(1, report)

    // 10 time
    await runProgram(10, report)

    // 100 time
    await runProgram(100, report)

    fs.writeFileSync('./result.json', JSON.stringify(report), { flag: 'w' })
}

async function runProgram(loop: number, destination: Trial[]) {
    let startTime = new Date()
    let startMem = os.freemem()

    for (let i = 0; i < loop; i++) {
        await listAllJsRecursive('/Users/malowong/tecky-exercises/Matthew')
    }

    let endTime = new Date()
    let endMem = os.freemem()
    let timeDifference = endTime.getTime() - startTime.getTime()
    let memDifference = startMem - endMem

    destination.push({
        startDate: startTime,
        endDate: endTime,
        timeNeeded: timeDifference,
        extraMemUsed: memDifference,
        name: `${loop} times`,
    })
}

interface Trial {
    //Think of what fields are necessary
    startDate: Date,
    endDate: Date,
    timeNeeded: number,
    extraMemUsed: number,
    name: string,
}

async function readTheReport() {

    // there is no need to use fs.readFile. use the promisified version instead. 
    let result = fs.readFileSync('./result.json')
    console.log(JSON.parse(result.toString('utf8')))

}

readCommand()