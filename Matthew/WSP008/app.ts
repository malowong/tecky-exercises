import express from "express"; // {res, req, next}
import { Request, Response, NextFunction } from "express";
import path from "path";
import expressSession from "express-session";
import jsonfile from "jsonfile";
import multer from "multer";

// define type

type Memo = {
  id: number;
  content: string;
  image?: string;
};

type Users = {
  username: string;
  password: string;
};

// make sure express handle the json file

const app = express();
app.use(express.json());

// you still need multer if you wish to upload files

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});
const upload = multer({ storage });

// getting memo data from the json and send it to front-end

app.get("/memo", async (req, res) => {
  const memo: Memo[] = await jsonfile.readFile(
    path.join(__dirname, "memo.json")
  );
  res.json({ data: memo });
});

// update memo WITH FILES (multer)

app.post("/memo", upload.single('image'), async (req, res) => {
  console.log(req.body.content);
  console.log(req.file?.filename);

  // read data from jsonfile
  const memo: Memo[] = await jsonfile.readFile(
    path.join(__dirname, "memo.json")
  );

  // add a new memo record to the array
  memo.push({

    // this is the best way to assign id number. 
    // if you have only memo.length - 1, you cannot handle a case where some id are deleted. 
    // only by getting the id of the last set of data + 1 will get you the satifying result.
    id: memo[memo.length - 1].id + 1,
    content: req.body.content,

    // remember to add ? here and the type, as it is optional for user to upload files (type = string OR undefined)
    image: req.file?.filename,
  });

  // save the data into jsonfile
  await jsonfile.writeFile(path.join(__dirname, "memo.json"), memo);

  res.json({ message: "success" });
});

// update new memo WITHOUT FILES (applicaion/JSON)

// app.post("/memo", async (req, res) => {
//   console.log(req.body.content);

//   const memo: Memo[] = await jsonfile.readFile(
//     path.join(__dirname, "memo.json")
//   );

//   memo.push({
//     id: memo[memo.length - 1].id + 1,
//     content: req.body.content,
//   });

//   await jsonfile.writeFile(path.join(__dirname, "memo.json"), memo);

//   res.json({ message: "success" });
// });

// login

app.use(
  expressSession({
    secret: "Tecky Academy teaches typescript",
    resave: true,
    saveUninitialized: true,
  })
);

app.post("/login", async (req, res) => {
  console.log(req.body);

  // get username and password from req.body.
  const { username, password } = req.body;

  // validation
  if (!username || !password) {
    res.status(400).json({ message: "invalid username or password" });
    return;
  }

  const login: Users[] = await jsonfile.readFile(
    path.join(__dirname, "users.json")
  );

  // instead of logging user input into the json (which is not making sense as we have to validate their input), 
  // this time we validate their input according to the user.json
  // the array.find function take in user as an param, 

  const user = login.find(function (user) {
    return user.username == username && user.password == password
  })
  
  // and we are not using the redirect here as well. instead we only return json and register session. 
  // all the alert and redirection will be handled by front-end.

  if (!user) {
    res.status(400).json({ message: "invalid username or password" });
    return
  }

  req.session["user"] = {username: user.username}
  res.json({ message: "success" })

});

const isLoggedIn = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.session["user"]) {
    //called Next here
    next()
    return
  } else {
    // redirect to index page
    res.redirect("/");
  }
};

// this is for debugging

app.use((req, res, next) => {
  const time = new Date().toISOString();
  console.log(`[INFO] ${time} req path: ${req.path} req method: ${req.method}`);
  next();
});

// middlewares for static file & 404
app.use(express.static(path.join(__dirname, "public")));

// to show the uploaded images on the browser, we have to add middleware for the uploads folder as well
app.use(express.static(path.join(__dirname, "uploads")));

// admin.html should be inside protected; it has to be below the public folder.
app.use(isLoggedIn, express.static(path.join(__dirname, "protected")));

app.use((req, res) => {
  res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`[info] listening to port ${PORT}`);
});
