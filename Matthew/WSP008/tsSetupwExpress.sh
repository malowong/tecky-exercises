npm init -y
npm install  ts-node typescript @types/node
touch .gitignore
cat >> .gitignore << EOF
node_modules
.DS_Store
EOF
touch tsconfig.json index.js app.ts
cat >> tsconfig.json << EOF
{
    "compilerOptions": {
        "module": "commonjs",
        "target": "es5",
        "lib": ["es6", "dom"],
        "strict": true,
        "sourceMap": true,
        "allowJs": true,
        "jsx": "react",
        "esModuleInterop":true,
        "moduleResolution": "node",
        "noImplicitReturns": true,
        "noImplicitThis": true,
        "noImplicitAny": true,
        "strictNullChecks": true,
        "suppressImplicitAnyIndexErrors": true,
        "noUnusedLocals": true
    },
    "exclude": [
        "node_modules",
        "build",
        "scripts",
        "index.js"
    ]
}
EOF
cat >> index.js << EOF
require('ts-node/register');
require('./app');
EOF
npm install express @types/express
npm install ts-node-dev
