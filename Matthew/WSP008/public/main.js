// we should:
// 1) build the route (back-end)
// 2) test the API (insomnia)
// 3) build the API call (front-end)


window.onload = async () => {
    await loadMemoData();
    submitFormwithFiles();
    login();
};

async function loadMemoData() {
    const response = await fetch("/memo");
    const memos = (await response.json()).data;

    // the console.log here will be displayed on browser. 
    console.log(memos);

    let htmlStr = ``;
    for (const memo of memos) {
        // to prevent the dead image icon, we can insert empty string if memo.image is undefined
        let image = memo.image ? `<img src="${memo.image}" alt="" srcset="">` : ``
        htmlStr += /*HTML*/ `
        <div class="box">
            <span class="text">${memo.content}</span>
            ${image}
        </div>
        `
    }
    document.querySelector('.memo-showcase').innerHTML = htmlStr
}

// you can use FormData (built-in type of javascript) if you want to upload files as well.

function submitFormwithFiles() {
    const form = document.getElementById("memo-submit")
    form.addEventListener("submit", async function (event) {

        // by using this, we can intercept the form submission event and extract the form submission data.
        // if we do not take control form the html submit form, the data would show on the address only, i.e. localhost:8080/?input:data+...
        event.preventDefault()

        const formData = new FormData()

        // the first parameter shd be the same as the syntax following req.body / req.file
        // the second parameter (i.e. form[abc]) shd be the same as the name of the html tag
        // it is better to have the same name throughout index.html to backend.

        formData.append('content', form['memoContent'].value)
        formData.append('image', form['image'].files[0])

        // the variable RESPONSE is used to catch the response from server

        const response = await fetch("/memo", {
            method: "POST",
            // no need for header as we are not using formData but not application/JSON
            body: formData,
        });

        if (response.status == 200) {
            window.location = "/index.html";

            // this is the secret of how to refresh the page right after submitting memos.
            // this can only be done by AJAX. if you use synchronous method you have to refresh your page.
            loadMemoData();
        }

    })
}

// if you using application/JSON with no need of uploading files:

// function submitForm() {
//     const form = document.getElementById("memo-submit");
//     form.addEventListener("submit", async function (event) {

//         event.preventDefault();

//         const contentObject = {};

//         contentObject.content = form["memoContent"].value;


//         const response = await fetch("/memo", {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json",
//             },

//             // stringify the object you just created to send to server ( JSON string is used to traverse between client and server)
//             body: JSON.stringify(contentObject),
//         });

//         if (response.status == 200) {
//             window.location = "/index.html";

//             loadMemoData();
//         }
//     });
// }

function login() {
    const form = document.getElementById("login");
    form.addEventListener("submit", async function (event) {

        event.preventDefault();

        const username = form["username"].value;
        const password = form["password"].value;

        const loginObject = { username, password };

        const response = await fetch("/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginObject),
        });

        if (response.status == 200) {
            window.location = "/admin.html";
        } else if (response.status == 400) {
            const errMessage = (await response.json()).message
            alert(errMessage)
        }
    });
}
