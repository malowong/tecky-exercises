import express from "express";

const app = express();

// req.query: the route handler would be trigger every time as the path pattern is /
// this moethod is suitable for optional input

app.get("/", (req, res) => {
  const name = req.query.name;
  const location = req.query.location;
  res.send(`Name is ${name}, Location is ${location}`);
});

// req.params: the route handler would be trigger only when the path pattern is corret
// the method is suitable for mandatory input

app.get("/:name/:location", (req, res) => {
  const name = req.params.name;
  const location = req.params.location;
  res.send(`Name is ${name}, Location is ${location}`);
});

// HTML methods: get, put (update), post (create), delete

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`[info] listening to port ${PORT}`);
});
