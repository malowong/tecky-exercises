// map? set? 
// map is like the hash table. every data gets a unique from the hash function.
// set is object in essence. a value in the set only occur once. 

// eg
let set = {}

set["A"] =  1

console.log(set)

set["A"] =  2

console.log(set) // value override
