// migration is all about creating / altering the STRUCTURE of tables.
// seed is all about inserting / altering / deleting the DATA of tables.

import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {

    // it is good to use transaction here. coz if anything go wrong we can stop the entire process by not committing the changes.
    // remember that all the knex is "TRX" under transaction. 

    const trx = await knex.transaction()

    try {

        // Deletes ALL existing entries

        // you have to delete at first in order to get a correct result. we use truncate here. 
        // it is also okay to use knex(file).del() like below:

        // await knex("users").del();
        // await knex("memos").del();

        // if so, remember to delete tables according to their dependencies.
        // i.e. likes should be deleted first (it includes the fkey of memos and users)

        // but truncate is better coz you can do it in one knex line and reset its id. 
        // truncate it is also faster, as it will delete all without selecting. 

        await trx.raw(`TRUNCATE users, memos RESTART IDENTITY CASCADE`);

        // Inserts seed entries
        await trx.insert([
            { username: "admin@tecky.io", password: await hashPassword('admin') },
            { username: "gordon@tecky.io", password: await hashPassword('gordon') },
            { username: "jason@tecky.io", password: await hashPassword('jason') }
        ]).into("users");

        await trx.insert([
            {
                content: "hello this is 1st memo",
                image: "image-1633918646096.jpeg"
            }, 
            {
                content: "hello this is 2nd memo",
                image: "image-1633861500303.jpeg"
            }, 
            {
                content: "hello this is 3rd memo",
                image: "image-1633683676261.jpeg"
            }
        ]).into("memos");

        // remember to commit!!!
        await trx.commit()

    } catch (error) {
        console.error(error.message);
        await trx.rollback();
    } finally {
        // we have to destroy if we want knex to stop working. remember it's knex.destroy not trx.destroy.
        await knex.destroy();
    }
    

};


// if you have multiple seed files, better to have number in front of filename (ie 00, 01, 02) WITH 00 as a reset seed file (del all tables in correct order)
