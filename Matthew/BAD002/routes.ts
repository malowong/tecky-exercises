import { Router } from "express";
import { io, knex } from "./app";
import { isLoggedIn } from './guards';
export const routes = Router();

// memoRoutes

import { MemoService } from "./services/memoService";
import { MemoController } from "./controllers/memoController";

const memoService = new MemoService(knex);
export const memoController = new MemoController(memoService, io);

import { memoRoutes } from "./routers/memoRoutes";
routes.use("/memos", isLoggedIn, memoRoutes);


// userRoutes

import { UserService } from "./services/userService";
import { UserController } from "./controllers/userController";

const userService = new UserService(knex);
export const userController = new UserController(userService);

import { userRoutes } from "./routers/userRoutes";
routes.use("/user", userRoutes);


// likeRoutes

import { LikeService } from "./services/likeService";
import { LikeController } from "./controllers/likeController";

const likeService = new LikeService(knex);
export const likeController = new LikeController(likeService, io);

import { likeRoutes } from "./routers/likeRoutes";
routes.use("/like", likeRoutes);
