import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('memos')){
        await knex.schema.alterTable('memos', (table) => {
            table.renameColumn("content-renamed", "content")
            table.string('image').alter()
        })
    }
}


export async function down(knex: Knex): Promise<void> {

    await knex("users").del();
    await knex("memos").del();
    if(await knex.schema.hasTable('memos')){
        await knex.schema.alterTable('memos', (table) => {
            table.renameColumn("content", "content-renamed")
            table.decimal('image', 3).alter()
        })
    }
}