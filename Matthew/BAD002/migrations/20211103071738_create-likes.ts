// migration is all about creating / altering the STRUCTURE of tables.
// seed is all about inserting / altering / deleting the DATA of tables.


import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    if(!await knex.schema.hasTable('users')){
        await knex.schema.createTable('users', (table) => {
            table.increments();
            table.string('username').notNullable()
            table.string('password').notNullable()
            table.timestamps(false,true);
        })
    }

    if(!await knex.schema.hasTable('memos')){
        await knex.schema.createTable('memos', (table) => {
            table.increments();
            table.string('content')
            table.string('image')
            table.timestamps(false,true);
        })
    }

    if(!await knex.schema.hasTable('likes')){
        await knex.schema.createTable('likes', (table) => {
            table.increments();
            table.integer('user_id').unsigned()
            table.foreign('user_id').references('users.id')
            table.integer('memo_id').unsigned()
            table.foreign('memo_id').references('memos.id')
            table.timestamps(false,true);
        })
    }

}


export async function down(knex: Knex): Promise<void> {

    // have to delete like table first, coz it have the fkey of users and memos.

    await knex.schema.dropTable('likes')
    await knex.schema.dropTable('memos')
    await knex.schema.dropTable('users')
}

