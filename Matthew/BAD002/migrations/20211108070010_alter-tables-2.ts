import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable('users')){
        await knex.schema.alterTable('users', (table) => {
            table.dropNullable('username');
            table.dropNullable('password');
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable('users')){
        await knex.schema.alterTable('users', (table) => {
            table.setNullable('username',);
            table.setNullable('password');
        })
    }
}

// it didn't work. finally i rollbacked all migration and change the first migration file (create_likes)

