import { Knex } from 'knex';

export class LikeService {
    constructor(private knex: Knex){}

    async getLikes(id: string) {
        return await this.knex('likes').count('memo_id').where('memo_id', id)
    }

    async postLikes(body: {user_id: number, memo_id: number}) {

        const {user_id, memo_id} = body

        return await this.knex('likes').insert({
            user_id, memo_id,
        }).returning('id')
    }

}