
import { Knex } from 'knex';

export class UserService {
    constructor(private knex: Knex){}

    async login(body: { username: string, password: string }) {
        const { username } = body;

        console.log(username)

        return await this.knex('users').where('username', username)
    }

}