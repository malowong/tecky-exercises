import { Memos } from '../models';
import { Knex } from 'knex';
export class MemoService {
    constructor(private knex: Knex){}

    async getMemos() {
        return await this.knex('memos').orderBy('id', 'desc')
    }

    async postMemos(body: Memos) {

        const {content, image} = body

        await this.knex('memos').insert({content, image})
    }

    async deleteMemos(id: number) {

        await this.knex('memos').del().where('id', id)

    }

}

// knex equation: this.knex(tableName).action().condition().condition()