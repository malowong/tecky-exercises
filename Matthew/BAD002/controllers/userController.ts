import { Request, Response } from "express"
import { checkPassword } from '../hash';
import { UserService } from "../services/userService"

export class UserController {
    constructor(private service: UserService) {}

    login = async (req: Request, res: Response) => {
        console.log(req.body)

        const { username, password } = req.body;

        const users = await this.service.login(req.body)

        const user = users[0]
        
        if (user == undefined) {
            return res.status(400).json({ message: "invalid username" });
        }
        const match = await checkPassword(password, user.password);
        if (match) {
            if (req.session) {
                req.session['user'] = {
                    id: user.id,
                    username: username,
                };
            }
            return res.json({ message: "success" });
        } else {
            return res.status(400).json({ message: "invalid password" });
        }
    }

    logout = async (req: Request, res: Response) => {

        if (req.session) {
            delete req.session['user'];
        }
        res.redirect('/index.html');
    }

}