import { MemoService } from "../services/memoService"
import { Request, Response } from "express"
import { Memos } from "../models"
import { Server as SocketIO } from 'socket.io';

export class MemoController {

  // remember that socket.io is also one of the constructor
  constructor(private service: MemoService, private io: SocketIO) {}

  getMemos = async (req: Request, res: Response) => {
    const memos = await this.service.getMemos();
    res.json({ data: memos })
  }

  postMemos = async (req: Request, res: Response) => {

    const memo: Memos = {
      content: req.body.content,
      image: req.file?.filename,
    }

    await this.service.postMemos(memo)
    this.io.emit("update-html", "New Memo Update");
    res.json({ message: "success" })
  }

  deleteMemos = async (req: Request, res: Response) => {

    const memoID = parseInt(req.params.id)

    if(isNaN(memoID)){
      res.status(400).json({ message: "Invalid Memo ID" })
      return
    }

    await this.service.deleteMemos(memoID)

    this.io.emit("update-html", "Memo Deleted");

    res.json({ message: "success" })

  }

}