import { LikeService } from "../services/likeService"
import { Request, Response } from "express"
import { Server as SocketIO } from 'socket.io';
export class LikeController {
  constructor(private service: LikeService, private io: SocketIO) {}

  getLikes = async (req: Request, res: Response) => {

    const likes = (await this.service.getLikes(req.params.mid))[0].count

    res.json({ data: likes })
  }

  postLikes = async (req: Request, res: Response) => {

    try {
      const likes = {
        user_id: req.session['user'].id,
        memo_id: parseInt(req.params.mid),
      }
  
      await this.service.postLikes(likes)
      this.io.emit("update-html", "New Like");
      res.json({ message: "success" })
      
    } catch (error) {
      console.error(error)
      res.status(400).json({ message: "like fail" })
    }

  }
  
}