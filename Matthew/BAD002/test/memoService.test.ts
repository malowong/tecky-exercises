// Test Target: memoService
// Mock Target: io


// do a dummy test first

it("dummy testcase", () => {
    expect(1 + 1).toBe(2);
})

// for testing service, you have to:
// 1. create testing database
// 2. migrate testing environment
// 3. don't have to seed coz we can seed by testing here.

// this is a test case for both service and knex. 
// if you want to test service alone, you have to mock knex as well which is a bit difficult.

import { MemoService } from "../services/memoService";
import Knex from "knex"

const knexConfigs = require('../knexfile');
const knex = Knex(knexConfigs["test"])

describe('test memoService', () => {
    let service: MemoService;

    beforeEach(async () => {
        service = new MemoService(knex)

        // like seeding, delete all data before starting.
        await knex.raw(`TRUNCATE memos RESTART IDENTITY CASCADE`);
        await knex.insert([
            {
                content: "1st test memo",
            },
            {
                content: "2nd test memo",
                image: "testing.jpeg"
            }
        ]).into("memos");

    })

    it("testing getMemos success", async () => {
        const result = await service.getMemos();
        expect(result.length).toBe(2);
        expect(result[0].content).toBe("2nd test memo")
    });

    afterAll(async () => {
        // like seeding, remember to destroy after all.
        await knex.destroy()
    })

})