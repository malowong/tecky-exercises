// Test Target: memoController
// Mock Target: memoService, io

// do a dummy test first

// it("dummy testcase", () => {
//     expect(1+1).toBe(2);
// })

import { MemoController } from '../controllers/memoController'
import { MemoService } from '../services/memoService'
import { Request, Response } from 'express'
import { Server as SocketIO } from 'socket.io';

// we have to mock everything besides the protagonist.
// why do we need to mock?
// jest.mock('../services/memoService')

describe('test memoController', () => {

    // create all variables, have to add types
    let controller: MemoController;
    let service: MemoService;
    let req: Request;
    let res: Response;
    let io: SocketIO;
    
    // environment setting, assign all used function in all variables
    beforeEach(() => {
        service = new MemoService({} as any)

        // suppose you should mock all service that is used in the memoContoller.
        service.getMemos = jest.fn(() => Promise.resolve([
            { id: 1, content: "foo" },
            { id: 2, content: "bar" }
        ]))

        service.deleteMemos = jest.fn(() => Promise.resolve())

        service.postMemos = jest.fn((memos) => Promise.resolve())

        req = {
            body: {},
            params: {},
            // file: {},
        } as Request

        res = {
            // for res.status(400).json, the status cannot be a empty function.
            // here we return res to the status, such that it become a loop and thus turn to json empty function.
            status: jest.fn(() => res),
            json: jest.fn(),
        } as any as Response

        // when we mock io, remember to insert the mocked io into the mocked controller. 

        io = {
            emit: jest.fn(),
        } as any as SocketIO

        controller = new MemoController(service, io)

    })

    it('test getMemos success', async () => {
        await controller.getMemos(req, res)
        expect(service.getMemos).toBeCalled()
        expect(res.json).toBeCalledWith({ data: [
            { id: 1, content: "foo" },
            { id: 2, content: "bar" }
        ]})
    })

    it('test postMemos success', async () => {
        req.body.content = "test content"
        // since the filename may not be existed (multer.file | undefined), we use {}
        req.file = {filename: "test filename"} as Express.Multer.File

        const memo = {
            content: req.body.content,
            image: req.file?.filename,
        }

        await controller.postMemos(req, res)
        expect(service.postMemos).toBeCalledWith(memo)
        expect(io.emit).toBeCalledWith("update-html", "New Memo Update")
        expect(res.json).toBeCalledWith({ message: "success" })
    })

    it('test deleteMemos fail - invallid memo ID', async () => {
        req.params.id = "error"
        await controller.deleteMemos(req, res)
        expect(res.status).toBeCalledWith(400)
        expect(res.json).toBeCalledWith({ message:  "Invalid Memo ID" })
        expect(service.deleteMemos).not.toBeCalled()
    })


})


