// 1. "this" in function: global
// the scope is bound to the global one by default

function func1() {
    console.log("1a", this) // global
}

func1()

// 1b. "this" in arrow function: global
// arrows functions do not have their own "this" but they inherit it from the parent scope, in this case the global one. this is is called "lexical scoping".

const func2 = () => {
    console.log("1a", this);
};
  
func2();

// 2. "this" in function as method in object: object

const object1 = {
    a: function video() {
        console.log("2", this) // object1
    },
    b: 10,
}

object1.a()

// 3. "this" in arrow function as method in object: parent scope (global)
// arrow functions do not have their own "this". instead, they inherit the one from the parent scope, which is called "lexical scoping".

const object2 = {
    a: () => {
        console.log("3", this) // global
    },
    b: 10,
}

object2.a()

// 4. "this" in arrow function in regular function as method in object: object3 by showTags()
// the regular function showTags binds "this" to the "object3" context.
// Since the arrow function doesn't have its own binding and forEach (as a function call) doesn't create a binding tself, 
// the "object3" context of the traditional function will be used within.

const object3 = {
    title: "Jason",
    tags: ["a", "b", "c"],
    showTags() {
        this.tags.forEach((tag) => {
            console.log(this) // object3
        })
    }
}

object3.showTags()

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions#arrow_functions_used_as_methods

// https://www.codementor.io/@dariogarciamoya/understanding-this-in-javascript-with-arrow-functions-gcpjwfyuc

// 5a. "this" in arrow functions in regular ASYNC function as method in object: myObject by async function()
// Arrow functions are a good choice for API calls ( asynchronous code ), only if we use CLOSURES
// we ask to do something async, we wait for the answer to do some actions and we don't have to worry about the scope we were working with.

myObject = {
    myMethod: async function () {
      helperObject.doSomethingAsync('superCool', () => {
        console.log(this); // this === myObject
      });
    },
};


// num = 100

// var obj = { // does not create a new scope
//     i: 10,
//     showTags: () => {
//         console.log(this.num)
//     },
//     b: () => console.log(this.i, this),
//     c: function() {
//       console.log(this);
//     }
//   }
  
// //   obj.b(); // prints undefined, Window {...} (or the global object)
//   obj.showTags(); // prints 10, Object {...}
//   obj.c(); 