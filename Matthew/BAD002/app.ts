// best practice: put dotenv on top as ts will not notify you about that.
// dotenv + knex

import Knex from 'knex';
import dotenv from 'dotenv';
dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
export const knex = Knex(knexConfig)

import express from "express";
import http from 'http';
import { Server as SocketIO } from 'socket.io';
// since you are using knex, you do not have to import pg anymore. but you still have to download the pg package as knex need it.
// import pg from 'pg';
import expressSession from 'express-session';
import multer from "multer";
import path from "path";

const app = express();

// json file handle
app.use(express.json());

// database config
// export const client = new pg.Client({
//     database: process.env.DB_NAME,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD
// })

// this is crucial for routes.ts. must be used before import routes
// BUT! if you use knex you do not need this anymore. 
// client.connect()

//socket.io config
const server = new http.Server(app);
export const io = new SocketIO(server);


// session config
let sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true,
});

app.use(sessionMiddleware);

io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res!;
    sessionMiddleware(req, res, next as express.NextFunction);
});

io.on('connection', function (socket) {
    console.log("socket ok")
    // if (!socket.request.session.user) {
    //     socket.disconnect()
    // }
});

// multer config

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
export const upload = multer({ storage });

// testing 

app.use((req, res, next) => {
    console.log(`[INFO] path: ${req.path}, method: ${req.method}`)
    next();
})

// REMEMBER: any import from your local files must be sticking to their usage.
// for this case, you can not connect to your client if you import routes (which also call client) before client.connect().
import { routes } from './routes'
app.use('/', routes);
// app.use('/', buttonRoutes);
// app.use('/memo/:mid', isLoggedIn, buttonRoutes);
// app.use('/like/:mid', isLoggedIn, buttonRoutes);


app.use(express.static('public'));
import { isLoggedIn } from './guards';
app.use(isLoggedIn,express.static('frontend'));
app.use(express.static("uploads"));

app.use((req, res) => {
    res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;

// remember it is server.listen not app.listen!
server.listen(PORT, () => {
    console.log(`[info] listening to port ${PORT}`);
})