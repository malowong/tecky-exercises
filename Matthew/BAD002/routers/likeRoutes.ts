import express from "express";
import { likeController } from "../routes";

export const likeRoutes = express.Router();

// Route handler

likeRoutes.get("/:mid", likeController.getLikes);

likeRoutes.post("/:mid", likeController.postLikes);