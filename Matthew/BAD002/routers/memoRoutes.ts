import express from "express";
import { memoController } from "../routes";
import { upload } from "../app"

export const memoRoutes = express.Router();

// Route handler

memoRoutes.get("/", memoController.getMemos);

memoRoutes.post("/", upload.single('image'), memoController.postMemos)

memoRoutes.delete("/:id", memoController.deleteMemos);