import express from 'express';
// import { checkPassword } from './hash';
// import { client } from './app';

import { userController } from "../routes"

export const userRoutes = express.Router();

userRoutes.post("/login", userController.login)

userRoutes.post("/logout", userController.logout)



// userRoutes.post('/login', login);

// async function login(req: express.Request, res: express.Response) {
//     const { username, password } = req.body;
//     console.log(req.body);
//     const users = (await client.query(`SELECT * FROM users WHERE username = $1`, [username])).rows;
//     const user = users[0];
//     console.log(user)
//     if (user == undefined) {
//         return res.status(400).json({ message: "invalid username" });
//     }
//     const match = await checkPassword(password, user.password);
//     if (match) {
//         if (req.session) {
//             req.session['user'] = {
//                 id: user.id
//             };
//         }
//         console.log("success!")
//         return res.redirect('/')
//         // how to go to the protect pages?
//     } else {
//         return res.status(400).json({ message: "invalid password" });
//     }
// }

// userRoutes.post('/logout', logout);

// async function logout(req: express.Request, res: express.Response) {
//     if (req.session) {
//         delete req.session['user'];
//     }
//     res.redirect('/index.html');
// }