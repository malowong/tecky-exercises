import { combineReducers, createStore } from "redux"
import { gameReducer, IGameState } from "./redux/game/reducers";
import { ILeaderboardState, leaderboardReducer } from "./redux/leaderboard/reducers";


export interface IRootState {
    game: IGameState,
    leaderboard: ILeaderboardState,
}

const reducer = combineReducers({ 
    game: gameReducer,
    leaderboard: leaderboardReducer,
})


// to use redux debugger on browser, we have to change the createStore function to the below:
declare global {
    /* tslint:disable:interface-name */
    interface Window {
       __REDUX_DEVTOOLS_EXTENSION__: any
    }
}

export const store = createStore(reducer, 
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);