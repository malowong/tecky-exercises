import { addPlayer } from "./actions"
import { gameReducer, IGameState } from "./reducers"

describe('Game reducer', () => {
    it('should add new player', () => {
        const initialState: IGameState = {
            players: [],
            progress: {},
            losers: [],
            canMove: false,
        }

        const action = addPlayer(1, "Matthew", "#ffcc00")
        
        const result = gameReducer(initialState, action)

        expect(result).toEqual({
            ...initialState,
            players: [...initialState.players, {
                id: 1,
                name: "Matthew",
                color: "#ffcc00",
            }]
        })
    })
})