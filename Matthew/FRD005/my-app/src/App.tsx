import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addPlayer } from "./redux/game/actions";
import "./App.css";
import { IRootState } from "./store";

function App() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [color, setColor] = useState("");

  // this is for react to communicate with redux (store.ts)
  const dispatch = useDispatch();

  // grab state from redux to show.
  const players = useSelector((state: IRootState) => state.game.players);

  return (
    <div className="App">
      <form
        onSubmit={(e) => {
          e.preventDefault();

          // once the form is submitted, we let redux to handle the state change
          dispatch(addPlayer(parseInt(id), name, color));
        }}
      >
        <h1>Add Player</h1>
        <div>
          <label>
            Id: <input type="text" value={id} onChange={(e) => setId(e.target.value)} />
          </label>
        </div>
        <div>
          <label>
            Name: <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
          </label>
        </div>
        <div>
          <label>
            Color: <input type="text" value={color} onChange={(e) => setColor(e.target.value)} />
          </label>
        </div>
        <div>
          <input type="submit" />
        </div>
      </form>
      <div>
        {players.map((player) => (
          // remember to add a key in every mapped div.
          <div key={player.id}>{player.name}</div>
        ))}
      </div>
    </div>
  );
}

export default App;
