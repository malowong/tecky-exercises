-- it is better to write down all your sql command here and copy to terminal.


SELECT * FROM memos;


-- Memo wall DB example

CREATE DATABASE memo-wall;

CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE memos(
    id SERIAL primary key,
    content TEXT not null,
    image VARCHAR(255),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users (username, password) VALUES ('jason', 'jason');
INSERT INTO memos (content) VALUES ('hello, world 1');
INSERT INTO memos (content) VALUES ('hello, world 2');
INSERT INTO memos (content) VALUES ('hello, world 3');


-- like TABLE (a many to many example)

CREATE TABLE likes (
    id SERIAL PRIMARY KEY,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    memo_id integer,
    FOREIGN KEY (memo_id) REFERENCES memos(id)
);

INSERT INTO likes (user_id, memo_id) VALUES (1,1)
INSERT INTO likes (user_id, memo_id) VALUES (1,2)
INSERT INTO likes (user_id, memo_id) VALUES (3,1)

-- like table are the bridge between the two tables, thus you have to join like into the two original tables as well
-- users are the left table.
-- 2nd left join: the joined table LEFT JOIN memos.
-- table structure: user --- like --- memos

SELECT * FROM users
LEFT JOIN likes 
ON users_id = likes.user_id
LEFT JOIN memos
ON memos = like.memo_id
