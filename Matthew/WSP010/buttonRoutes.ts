import express from "express";
import { client } from './app';
import { io } from './app';


export const buttonRoutes = express.Router();

// question: when should i add the try catch? every api?

// delete memo WITH QUERY.PARAMS

buttonRoutes.delete("/memo/:mid", async (req, res) => {
    const memoID = parseInt(req.params.mid)
    if (isNaN(memoID)) {
        res.status(400).json({ message: "invalid ID " })
        return
    }
    await client.query('DELETE FROM memos WHERE id=$1', [memoID])

    io.emit("update-html", "Memo Deleted");

    res.json({ message: "success" });
})

// update memo WITH QUERY.PARAMS

buttonRoutes.put("/memo/:mid", async (req, res) => {

    const memoID = parseInt(req.params.mid)
    const update = req.body.content
    console.log(memoID, update)
    if (isNaN(memoID)) {
        res.status(400).json({ message: "invalid ID " })
        return
    }
    await client.query('UPDATE memos SET content=$1 where id=$2', [update, memoID])

    io.emit("update-html", "Memo Updated");

    res.json({ message: "success" });
})

// like button


buttonRoutes.put("/like/:mid", async (req, res) => {
    console.log(req.session["user"].id)

    try {

        const memoID = parseInt(req.params.mid)
        const userID = req.session["user"].id

        await client.query('INSERT INTO likes (user_id, memo_id) VALUES ($1,$2)', [userID, memoID])
        
        res.json({ message: "success" })

    } catch (error) {
        console.error(error)
        res.status(400).json
    }

})

// get numbers of likes
// buttonRoutes.get("/", async (req, res) => {
//     const result = await client.query(`
//     SELECT memos.* from users 
//     INNER JOIN likes on likes.user_id = users.id 
//     INNER JOIN memos on likes.memo_id = memos.id`);
//     res.json({ data: result.rows });
// })


