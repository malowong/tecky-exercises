import express from "express";
import { client } from './app';
import multer from "multer";
import path from "path";
import { io } from './app';


export const memoRoutes = express.Router();

// define type

type Memos = {
    content: string;
    image?: string;
};

// multer

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
const upload = multer({ storage });


// getting memo data FROM DATABASE and send it to front-end

memoRoutes.get("/memo", async (req, res) => {
    // ORDER BY id DESC help us to get the latest memo to the top.

    const result = await client.query('SELECT * FROM memos ORDER BY id DESC');

    res.json({ data: result.rows });

});

// update memo WITH DATABASE


memoRoutes.post("/memo", upload.single('image'), async (req, res) => {
    try {
        console.log(req.body.content);
        console.log(req.file?.filename);

        const memo: Memos = {
            content: req.body.content,
            image: req.file?.filename,
        }

        await client.query('INSERT INTO memos (content,image,created_at,updated_at) values ($1,$2,now(),now())', [memo.content, memo.image])

        // broadcast new memo to all users by socket.io

        io.emit("update-html", "New Memo Update");

        res.json({ message: "success" });

    } catch (error) {
        console.error(error)
        res.status(400).json
    }

});