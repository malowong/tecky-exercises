window.onload = () => {
    // socket.io
    const socket = io.connect();
    
    loadMemoData();
    submitFormwithFiles();
    // login();
    
    // socket.io
    socket.on("update-html", async (msg) => {
        console.log(msg)
        loadMemoData()
    })

}

// by using socket.io, every user will get instant update without f5.
// only you will get instant update if you are not using socket.io

// the msg is "updateMessage" from app.ts


async function loadMemoData() {
    const response = await fetch("/memo");
    // method of fetch is "GET" by default.
    const memos = (await response.json()).data;
    console.log(memos)

    let htmlStr = ``;
    for (const memo of memos) {
        // to prevent the dead image icon, we can insert empty string if memo.image is undefined
        let image = memo.image ? `<img src="${memo.image}" alt="" srcset="">` : ``
        htmlStr += /*HTML*/ `
        <div id="memo-${memo.id}" class="box" contenteditable="true">
            <span class="text">${memo.content}</span>
            ${image}
            <div class="delete-button" onclick="deleteMemo(${memo.id})">
                <i class="bi bi-trash"></i>
            </div>
            <div class="edit-button" onclick="updateMemo(${memo.id})">
                <i class="bi bi-pencil"></i>
            </div>
            <div class="like-button" onclick="likeMemo(${memo.id})">
                <i class="bi bi-hand-thumbs-up"></i>
                <span>0 like</span>
            </div>
        </div>
        `
    }
    document.querySelector('.memo-showcase').innerHTML = htmlStr
}

async function deleteMemo(memoID) {
    const response = await fetch(`/memo/${memoID}`, {
        method: "DELETE"
    })

    if (response.status == 200) {
        loadMemoData();
    }
}

// you cannot use formData here as you are not passing a form to back-end. use applicaton/json instead.
async function updateMemo(memoID) {
    const memo = document.getElementById(`memo-${memoID}`)

    const updateObject = {}

    updateObject.content = memo.textContent

    const response = await fetch(`/memo/${memoID}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(updateObject),
    })

    if (response.status == 200) {
        loadMemoData();
    }
}

async function likeMemo(memoID) {


    const response = await fetch(`/like/${memoID}`, {
        method: "PUT",
    })

    if (response.status == 200) {


        loadMemoData();
        console.log("success")

    }
}


function submitFormwithFiles() {
    const form = document.getElementById("memo-submit")
    form.addEventListener("submit", async function (event) {

        event.preventDefault()

        const formData = new FormData()

        formData.append('content', form['memoContent'].value)
        formData.append('image', form['image'].files[0])

        // the variable RESPONSE is used to catch the response from server

        const response = await fetch("/memo", {
            method: "POST",
            body: formData,
        });

        if (response.status == 200) {

            // if there is no window.location the page will not be refreshed.

            loadMemoData();
        }

    })
}

// function login() {
//     const form = document.getElementById("login");
//     form.addEventListener("submit", async function (event) {

//         event.preventDefault();

//         const username = form["username"].value;
//         const password = form["password"].value;

//         const loginObject = { username, password };

//         const response = await fetch("/login", {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json",
//             },
//             body: JSON.stringify(loginObject),
//         });

//         if (response.status == 200) {
//             // window.location = "/admin.html";

//             htmlStr = /*html*/ `
//             <h3>管理員</h3>
//             <span>你已登入</span><br>
//             <input type="submit" value="登出"/>
//             `

//             document.querySelector('.loginPanel').innerHTML = htmlStr

//         } else if (response.status == 400) {
//             const errMessage = (await response.json()).message
//             alert(errMessage)
//         }
//     });
// }