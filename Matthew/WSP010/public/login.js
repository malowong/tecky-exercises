window.onload = function(){
    login();
    const searchParams = new URLSearchParams(window.location.search);
    const errMessage = searchParams.get('error');

    if(errMessage){
        const alertBox = document.createElement('div');
        alertBox.classList.add('alert','alert-danger');
        alertBox.textContent = errMessage;
        document.querySelector('#error-message').appendChild(alertBox);
    }else{
        document.querySelector('#error-message').innerHTML = "";
    }
}

function login() {
    const form = document.getElementById("login");
    form.addEventListener("submit", async function (event) {

        event.preventDefault();

        const username = form["username"].value;
        const password = form["password"].value;

        const loginObject = { username, password };

        const response = await fetch("/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginObject),
        });

        if (response.status == 200) {
            window.location = "/index.html";

        } else if (response.status == 400) {
            const errMessage = (await response.json()).message
            alert(errMessage)
        }
    });
}