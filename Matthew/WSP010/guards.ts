import {Request,Response,NextFunction} from 'express';

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        console.log("session ok")
        next();
    }else{
        res.redirect('/login.html');
        console.log("session not ok")
    }
}