// best practice: put dotenv on top as ts will not notify you about that.

import dotenv from 'dotenv';
dotenv.config();

import express from "express";
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import path from "path";
import pg from 'pg';
import expressSession from 'express-session';


// login

import { isLoggedIn } from './guards';
import { userRoutes } from './userRoutes'
import { memoRoutes } from './memoRoutes';
import { buttonRoutes } from './buttonRoutes';


const app = express();

// json file handle
app.use(express.json());


// database config
export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

//socket.io config
const server = new http.Server(app);
export const io = new SocketIO(server);



client.connect() // "dial-in" to the postgres server
// no need to end once it has been connected


// HTML METHODS has all been moved to different routes.

// fyr: login WITH DATABASE (without bcrypt.js)

// app.post("/login", async (req, res) => {
//     try {
//         console.log(req.body);

//         const { username, password } = req.body;


//         // too repetitive, pls advise better solution.

//         const usernameResult = await client.query('SELECT * from users where username = $1', [username]);
//         const passwordResult = await client.query('SELECT * from users where password = $1', [password]);

//         console.log(usernameResult.rows[0], passwordResult.rows[0])

//         if (usernameResult.rows[0] == undefined || passwordResult.rows[0] == undefined) {
//             res.status(400).json({ message: "invalid username or password" });
//             return
//         }

//         req.session["user"] = { username: usernameResult.rows[0].username }
//         res.json({ message: "success" })

//         console.log(req.sessionID)

//     } catch (error) {
//         console.error(error)
//         res.status(400).json
//     }

// });

// const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
//     if (req.session["user"]) {
//         //called Next here
//         next()
//         return
//     } else {
//         // redirect to index page
//         res.redirect("/");
//     }
// };

// admin.html should be inside protected; it has to be below the public folder.
// app.use(isLoggedIn, express.static(path.join(__dirname, "protected")));

let sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true,
});

app.use(sessionMiddleware);

io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res!;
    sessionMiddleware(req, res, next as express.NextFunction);
});

io.on('connection', function (socket) {
    console.log("socket ok")
    // if (!socket.request.session.user) {
    //     socket.disconnect()
    // }
});

// question: when should i use the guard in order to prevent data leak?

app.use('/', userRoutes);
app.use('/', memoRoutes);
app.use('/', buttonRoutes);
app.use('/memo', isLoggedIn, memoRoutes);
app.use('/memo/:mid', isLoggedIn, buttonRoutes);
app.use('/like/:mid', isLoggedIn, buttonRoutes);


app.use(express.static('public'));
app.use(isLoggedIn,express.static('frontend'));
app.use(express.static("uploads"));

app.use((req, res) => {
    res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;

// remember it is server.listen not app.listen!
server.listen(PORT, () => {
    console.log(`[info] listening to port ${PORT}`);
})