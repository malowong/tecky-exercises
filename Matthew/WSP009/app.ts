import pg from 'pg';
import dotenv from 'dotenv';
import XLSX from 'xlsx';


// database config
dotenv.config();
const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

type Memos = {
    content: string,
    image: string,
}

type Users = {
    username: string,
    password: string
}

const workbook = XLSX.readFile('./WSP009-exercise.xlsx')
const sheet_name_list = workbook.SheetNames
let memoSheet: Memos[] = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]])
let usersSheet: Users[] = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])

console.log(memoSheet[0])
console.log(usersSheet.length)

async function main(){

    await client.connect() // "dial-in" to the postgres server


    for (let i = 0; i < usersSheet.length; i++){
        
        const user: Users = {
            username:usersSheet[i].username,
            password:usersSheet[i].password
        };

        // do not use `` and ${} for input to prevent from sql injection, which people could inser "username; DROP TABLE" to destroy your database.
        await client.query('INSERT INTO users (username,password,created_at,updated_at) values ($1,$2,now(),now())', [user.username,user.password]) 
        
        const result = await client.query('SELECT * from users where username = $1',[user.username]);
        console.log(result.rows[0].username) // gordon
    }

    for (let i = 0; i < memoSheet.length; i++){
        
        const memo: Memos = {
            content:memoSheet[i].content,
            image: memoSheet[i].image
        };
        await client.query('INSERT INTO memos (content,image,created_at,updated_at) values ($1,$2,now(),now())', [memo.content,memo.image]) 
        
        const result = await client.query('SELECT * from memos where content = $1',[memo.content]);
        console.log(result.rows[0])
    }

    await client.end() // close connection with the database

}

main();