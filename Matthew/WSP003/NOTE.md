# TS project template

- [ ] init npm project

```Text
npm init -y
```

**remark:**

`-y` ans yes to all questions

- [ ] install packages for TS project

```Text
npm install  ts-node typescript @types/node
```

**Folder Structure:**

```Text
node_modules      package-lock.json package.json
```

- [ ] create and configure `.gitignore`

```Text
node_modules
package_lock.json
.DS_Store
```

- [ ] create 3 files: `tsconfig.json`, `index.js` and `app.ts`

```Text
touch tsconfig.json index.js app.ts

```

- [ ] configure `tsconfig.json`, `index.js` and `app.ts`

`tsconfig.json`:

```JSON
{
    "compilerOptions": {
        "module": "commonjs",
        "target": "es5",
        "lib": ["es6", "dom"],
        "sourceMap": true,
        "allowJs": true,
        "jsx": "react",
        "esModuleInterop":true,
        "moduleResolution": "node",
        "noImplicitReturns": true,
        "noImplicitThis": true,
        "noImplicitAny": true,
        "strictNullChecks": true,
        "suppressImplicitAnyIndexErrors": true,
        "noUnusedLocals": true
    },
    "exclude": [
        "node_modules",
        "build",
        "scripts",
        "index.js"
    ]
}
```

`index.js`:

```Javascript
require('ts-node/register');
require('./app');

```

`app.ts`:

```Typescript
console.log('hello, world!');
```
