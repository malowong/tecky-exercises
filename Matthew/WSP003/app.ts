import fs from "fs"
import Path from "path"

// ex1

function listAllJs(path: string) {

    fs.readdir(path, function (err, files) {
        if (err) {
            console.error(err)
        } else {
            files.forEach(function (file) {
                fs.stat(Path.join(path, file), function (err, stat) {
                    if (stat.isDirectory() != true && file.substring(file.length - 3) == ".js") {
                        console.log(`${path}/${file}`)
                    }

                })
            })
        }
    })
}

listAllJs('/Users/malowong/tecky-exercises/Matthew/Game of Life');

// ex2

function listAllJsRecursive(path: string) {
    fs.readdir(path, function (err, files) {
        if (err) {
            console.error(err)
        } else {
            files.forEach(function (file) {
                let finalPath = Path.join(path, file)
                fs.stat(finalPath, function (err, stat) {
                    if (stat.isDirectory() != true && file.substring(file.length - 3) == ".js") {
                        // base case
                        console.log(Path.join(path, file))
                    } else if (stat.isDirectory() == true) {
                        // recursive case
                        listAllJsRecursive(Path.join(path, file))
                    }

                })
            })
        }
    })
}

listAllJsRecursive('/Users/malowong/tecky-exercises/Matthew');