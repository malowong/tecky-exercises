const cards = [
    ['Spade', 'A'],
    ['Diamond', 'J'],
    ['Club', '3'],
    ['Heart', '6'],
    ['Spade', 'K'],
    ['Club', '2'],
    ['Heart', 'Q'],
    ['Spade', '6'],
    ['Heart', 'J'],
    ['Spade', '10'],
    ['Club', '4'],
    ['Diamond', 'Q'],
    ['Diamond', '3'],
    ['Heart', '4'],
    ['Club', '7']
]

function compareCard(cardA, cardB) {
    const ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    const suits = ["Diamond", "Club", "Heart", "Spade"];
    const [suitA, rankA] = cardA;
    const [suitB, rankB] = cardB;
    const ranksDiff = ranks.indexOf(rankA) - ranks.indexOf(rankB);
    if (ranksDiff !== 0) {
        return ranksDiff;
    } else {
        return suits.indexOf(suitA) - suits.indexOf(suitB);
    }
}

// count the number of card which is of suit Spade

console.log(cards.reduce(function (acc, elem) {
    if (elem[0] == "Spade") {
        return acc + 1
    } else {
        return acc
    }
}, 0))

// Remove all the card that is smaller than ['Club','3'] .

console.log(cards.filter(function (elem) {
    return compareCard(['Club', '3'], elem) < 0
}))

// Count the number of card which is of suit Diamond or Heart and with the rank larger than or equal to J.

console.log(cards.filter(function (elem) {
    return (elem[0] == "Diamond" || elem[0] == "Heart") && isNaN(elem[1])
}))

// Replace all of the cards with suit Club to suit Diamond, keeping the same rank.

let result = []
console.log(cards.map(function (card) {
    if (card[0] == "Club") {
        return card[0].replace("Club", "Diamond")
    } else {
        return card
    }
}))

// Replace all of the cards with rank A to rank 2. Keeping the same suit.

let result2 = []
console.log(cards.map(function (card) {
    if (card[1] == "A") {
        return card[1].replace("A", "2")
    } else {
        return card
    }
}))
