const data = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]


const hk = data[0];


// hk[key] instanceof Object will called array as well, coz array is also a kind of objects. 
// have to use join (i.e. line 94). if there is no instanceof Object, you will find that javasciprt
// knows to join the array to be string in console.log automatically.


function printObject(capital, object){
    for (let key in object) {
        console.log(capital + '_' + key + ": " + object[key])
    }
}

for (let key in hk) {
    let capital = key[0].toUpperCase() + key.substring(1)
    
    // instanceof can also be use for array. 
    // it is all about logic flow. the logic branch of your original answer is (1) object - object (2) else. 
    // this one is (1) array - object (2) object (3) else. more precise, more accurate. 

    // also, using function to replace those annoying fors is a good way to go. 
    
    if (hk[key] instanceof Array){
        if (hk[key][0] instanceof Object){

            // hardest part is here. it is hard if not difficult for you to use for-in. therefore you can use 
            // for-of instead, as languages & currencies is arrays. grab the item and then for-in again using 
            // printObject will do. 

            for (let item of hk[key])
            printObject(capital, item)
        } else {

            // use join.
            
            console.log(capital + ": " + hk[key].join(","))
        }
    } else if (hk[key] instanceof Object){
        printObject(capital, hk[key])
    } else {
        console.log(capital + ": " + hk[key]);
    }
}

// my answer: (9/10)

// for (let key in hk) {
//     let capital = key[0].toUpperCase() + key.substring(1)
    
//     if (hk[key] instanceof Object) {

//         for (let key2 in hk[key]) {
            
//             if (hk[key][key2] instanceof Object) {
//                 for (let key3 in hk[key][key2]) {
//                     console.log(capital + "_" + key3 + ": " + hk[key][key2][key3])
//                 }

//             } else {
//                 console.log(capital + "_" + key2 + ": " + hk[key][key2])
//             }
//         }
//     } else {
//         console.log(capital + ": " + hk[key]);
//     }
// }


// using recusion: (9/10)



function printInfo(hk, parentTitle){

    
    for(let key in hk){

        // how to include the parent title? 
        // add the 2nd parameter into the function, 
        // parentTitle

        let capital
        if (parentTitle) {
            capital = parentTitle + '_' + key
        } else {
            capital = key[0].toUpperCase() + key.substring(1)
        }

        if (hk[key] instanceof Array) {
            if (hk[key] instanceof Object) {
                for (let item of hk[key]) {
                    printInfo(item, capital)
                }
            } else {
                console.log(capital + ": " + hk[key].join(','))
            }
        } else if (hk[key] instanceof Object){
            printInfo(hk[key], capital)
        }else{
            console.log(capital + ": " + hk[key])
        }
    }
}

printInfo(hk)







