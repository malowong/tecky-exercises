// arrow function 1

function abc(num){
    return num + 100
}

let abc = num => num + 100



// arrow function 2

function def(){
    return num
}

let def = () => num

// arrow function 3

function (a, b){
    let chuck = 42;
    return a + b + chuck;
}
  
(a, b) => {
    let chuck = 42;
    return a + b + chuck; 
}
