// an example of closure

function createCalculator(){
    let sum = 0

    function add(num){
        sum = sum + num
        return sum
    }

    function subtract(num){
        sum = sum - num
        return sum
    }

    function multiply(num){
        sum = sum * num
        return sum
    }

    function divide(num){
        sum = sum / num
        return sum
    }

    function reset(){
        sum = 0
        return sum
    }

    // getValue is essential, coz if you use add sum as objec in the calculator (aka calculator.sum below) instead, 
    // the sum will always be 0 as it is a different sum than the one you use in those functions.

    function getValue(){
        return console.log(sum)
    }

    // if your key and value is the same in the object (aka add: add), you can simply write one. 

    let calculator = {
        add,
        subtract,
        multiply,
        divide,
        getValue,
        reset,
    }

    return calculator
}

let calculator1 = createCalculator()
calculator1.add(3)
calculator1.subtract(2)
calculator1.multiply(10)
calculator1.divide(5)
calculator1.getValue()
calculator1.reset()
calculator1.add(500)
calculator1.getValue()


let calculator2 = createCalculator()
calculator2.add(50)
calculator1.multiply(100)
