// ex1

import fs from "fs"
import Path from "path"
import util from "util"

function fsReaddirPromise(path: string) {
    return new Promise<string[]>(function (resolve, reject) {
        fs.readdir(path, function (err, files) {
            if (err) {
                reject(err)
                return
            }
            resolve(files);
        })
    })
}

// function fsStatPromise(path: string) {
//     return new Promise<fs.Stats>(function (resolve, reject) {
//         fs.stat(path, function (err, files) {
//             if (err) {
//                 reject(err)
//                 return
//             }
//             resolve(files);
//         })
//     })
// }

async function listAllJsRecursive(path: string) {
    try {
        let files = await fsReaddirPromise(path);

        for (let file of files) {
            // if you prefer to use util.promisify:
            // beware that not all of the callback function are able to use this method
            let fsStatPromise = util.promisify(fs.stat)
            let stat = await fsStatPromise(Path.join(path, file));

            if (stat.isDirectory() != true && file.substring(file.length - 3) == ".js") {
                // base case
                console.log(Path.join(path, file))
            } else if (stat.isDirectory() == true) {
                // recursive case
                listAllJsRecursive(Path.join(path, file))
            }
        }
    } catch (err) {
        console.error(err);
    }
}


// ex2

// 2.1 setTimeout function

// your goal is to create a promise function with a original function inside. 
// the parameter of the promise funciton is the input of your original function, i.e the data and the duration. 
// for fs.readdir it is path only. for fs.stat it is the joined path which direct to specific file.

function setTimeoutPromise(data: string, duration: number) {
    // you can have resolve only, without reject. 
    return new Promise(function (resolve) {
        // here you insert the original callback function that you want to promisify. you have to find ways to insert your parameters. 
        setTimeout(function () {
            resolve(data)
        }, duration)
    })
}

async function setTimeoutAsync() {
    let time = new Date().toISOString()
    let result = await setTimeoutPromise("hi", 1000)
    console.log(time, result)

    let result2 = await setTimeoutPromise("bye", 2000)
    console.log(time, result2)

    let result3 = await setTimeoutPromise("byebye", 3000)
    console.log(time, result3)
}



// 2.2 readline function

import readline from 'readline';

export function questionPromise(query: string) {
    return new Promise<string>(function (resolve) {
        const readLineInterface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        readLineInterface.question(query, (answer) => {
            resolve(answer)
            readLineInterface.close();
        })
    })
}

async function questionAsync() {
    let result = await questionPromise("What is your name? ");
    console.log(`Hello ${result}`)
    
}

// questionPromise("What is your name? ").then(function (answer){
//     console.log(`Hello ${answer}`)
// })

// 2.3 Papaparse

import papa from 'papaparse';

function papaPromise(filename: string) {
    return new Promise(function (resolve) {
        const file = fs.createReadStream(filename);
        papa.parse(file, {
            worker: true,
            complete: function (results) {
                resolve(results.data);
            }
        });
    })
}


async function papaAsync() {
    let result = await papaPromise('anycsv.csv')
    console.log(result)
}

// Overall

async function main() {
    await listAllJsRecursive('/Users/malowong/tecky-exercises/Matthew')
    await setTimeoutAsync()
    await questionAsync()
    await papaAsync()
}

main()