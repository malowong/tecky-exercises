# TS project template

-   [ ] make new directory

```Text
mkdir WSP004
```

-   [ ] copy markdown file

```Text
cp ../WSP003/NOTE.md .
```

-   [ ] init npm project

```Text
npm init -y
```

**remark:** `-y` ans yes to all questions

-   [ ] install packages for TS project

```Text
npm install ts-node typescript @types/node
```

**remark:** there should be 3 files, namely node_modules, package-lock.json, and package.json


-   [ ] create 4 files: `tsconfig.json`, `index.js`, `app.ts` and `.gitignore`

```Text
touch tsconfig.json index.js app.ts .gitignore
```

-   [ ] configure `.gitignore`

```Text
node_modules
package-lock.json
.DS_Store
```

-   [ ] configure `tsconfig.json`, `index.js` and `app.ts`

`tsconfig.json`:

```JSON
{
    "compilerOptions": {
        "module": "commonjs",
        "target": "es5",
        "lib": ["es6", "dom"],
        "sourceMap": true,
        "allowJs": true,
        "jsx": "react",
        "esModuleInterop":true,
        "moduleResolution": "node",
        "noImplicitReturns": true,
        "noImplicitThis": true,
        "noImplicitAny": true,
        "strictNullChecks": true,
        "suppressImplicitAnyIndexErrors": true,
        "noUnusedLocals": true
    },
    "exclude": [
        "node_modules",
        "build",
        "scripts",
        "index.js"
    ]
}
```

`index.js`:

```Javascript
require('ts-node/register');
require('./app');

```

`app.ts`:

```Typescript
console.log('hello, world!');
```