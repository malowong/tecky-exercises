// 1. findFactors function

function findFactors(num: number): number[] {
    let factors: number[] = [];
    for (let factor = 2; factor <= num / 2; factor++) {
        if (num % factor === 0) {
            factors.push(factor);
        }
    }
    return factors;
}

// 2. LeapYear function

function leapYear(year: number): boolean {
    if (year % 400 === 0) {
        console.log("Leap Year");
        return true;
    } else if (year % 100 === 0) {
        console.log("Not a Leap Year");
        return false;
    } else if (year % 4 === 0) {
        console.log("Leap Year");
        return true;
    } else {
        console.log("Not a Leap Year");
        return false;
    }
}

// 3. RNATranscription function

function rnaTranscription(dna: string): string {
    let rna = "";
    for (let nucleotide of dna) {
        switch (nucleotide) {
            case "G":
                rna += "C";
                break;
            case "C":
                rna += "G";
                break;
            case "T":
                rna += "A";
                break;
            case "A":
                rna += "U";
                break;
            default:
                throw new Error(`The nucleotide ${nucleotide} does not exist`)
        }
    }
    return rna;
}

// 4. the factorial recursive function

function factorial(number: number): number {
    if (number === 0 || number === 1) {
        return 1;
    }

    return factorial(number - 1) * number
}

// 5. 

type Teacher = {
    name: string,
    age: number,
    students: Student[],
}

type Student = {
    name: string,
    age: number,
    exercises?: Exercise[],
}

type Exercise = {
    score: number,
    date: Date,
}

const peter: Teacher = {
    name: "Peter",
    age: 50,
    students: [
        { name: "Andy", age: 20 },
        { name: "Bob", age: 23 },
        {
            name: "Charlie", 
            age: 25, 
            exercises: [{ score: 60, date: new Date("2019-01-05") }],
        },
    ],
}

// 

console.log('peter:', peter)
if (peter.students[2].exercises) {
    console.log('charlie exercise length:', peter.students[2].exercises.length)
}


// 6. setTimeout

// add ": () => void " after timeoutHandler to indicate that the const would return void.

// const timeoutHandler: () => void = () => {
//     console.log("Timeout happens!");
// }

const timeoutHandler: () => void = function() {
    console.log("Time...")
}

const timeout: number = 1000;

setTimeout(timeoutHandler, timeout);

// 7. someValue

const someValue: number | null = Math.random() > 0.5 ? 12 : null;
let sum  = someValue != null ? 12 + someValue : 12

// add an if-else clause to ensure that it won't error.
