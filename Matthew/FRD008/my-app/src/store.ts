import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { createBrowserHistory } from "history";
import { applyMiddleware, combineReducers, compose, createStore } from "redux"
import { authReducer } from "./redux/auth/reducers";
import { IAuthState } from "./redux/auth/state";
import { blogReducer, IBlogState } from "./redux/blog/reducers";
import { gameReducer, IGameState } from "./redux/game/reducers";
import { ILeaderboardState, leaderboardReducer } from "./redux/leaderboard/reducers";

export const history = createBrowserHistory()

// RouterState, connectRouter, history, compose are all syntax. no changes are allowed.

export interface IRootState {
    game: IGameState,
    leaderboard: ILeaderboardState,
    blog: IBlogState,
    auth: IAuthState,
    router: RouterState,
}

const reducer = combineReducers({ 
    game: gameReducer,
    leaderboard: leaderboardReducer,
    blog: blogReducer,
    auth: authReducer,
    router: connectRouter(history)
})


// to use redux debugger on browser, we have to change the createStore function to the below:
declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer, 
    composeEnhancers(
        // steps are important. thunk first, history last.
        applyMiddleware(thunk)
        applyMiddleware(routerMiddleware(history))
    )
);