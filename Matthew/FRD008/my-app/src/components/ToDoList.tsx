import { useEffect, useState, memo } from "react";
import Footer from "./Footer";
import ToDoItem from "./ToDoItem";

function ToDoList() {
  const [toDoItemArr, settoDoItemArr] = useState([]);


  const status = useSelector((state: IRootState) => state.todo.status)

  const { register, handleSubmit, reset } = useForm<TodoListForm>({
      defaultValues: {
          description: "".
      }
  })

  useEffect(() => {
      dispatch(getAllTodoItemsThunk())
  }, [dispatch])

  const filterItems = (items: TodoItemState[]) => {
      if (status === Status.Active) {
          return items.filter((item) => item.is_chacked === false)
      } else if (status === Status.Completed) {
          return items.filter((item) => item.is_chacked === true)
      }
    return items;
  }

  return (
    <>
      {toDoItemArr.map((toDoItem, idx) => (
        <div key={idx}>{toDoItem}</div>
      ))}
    </>
  );
}

export default function memo(Footer)

// if we use watch if useForm (react-hook-form), to prevent other unrelated components from rerendering
// during every input, we can use memo() to wrap the component and use that. in this case we use
// Footer (memo wrapped version) in Todolist.