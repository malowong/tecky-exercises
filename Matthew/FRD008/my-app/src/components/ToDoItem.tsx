import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

type Inputs = {
  toDoItems: string;
};

export default function ToDoItem() {
  const [toDoItemArr, settoDoItemArr] = useState<string[]>([]);

  // we use react-hook-form here to prevent the repetitive useState and setter.

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      toDoItems: "insert your todo items here",
    },
  });
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const newtoDoItemArr = toDoItemArr.slice();
    newtoDoItemArr.push(data.toDoItems);
    settoDoItemArr(newtoDoItemArr);
  };
  console.log(watch("toDoItems")); // watch input value by passing the name of it

  return (
    <>
      <h1>Todos</h1>
      {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* register your input into the hook by invoking the "register" function */}
        {/* include validation with required or other standard HTML validation rules */}
        <input {...register("toDoItems", { required: true })} />

        {/* errors will return when field validation fails  */}
        {errors.toDoItems && <span>This field is required</span>}

        <input type="submit" />
      </form>
      {toDoItemArr.map((toDoItem, idx) => (
        <div key={idx}>{toDoItem}</div>
      ))}
    </>
  );
}
