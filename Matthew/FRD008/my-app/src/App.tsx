import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import "./App.css";
import Footer from "./components/Footer";
import ToDoItem from "./components/ToDoItem";
import ToDoList from "./components/ToDoList";

// we have to use downgraded version of react-router-dom

function App() {
  
  return (
    <div className="App">
      <ToDoItem />
      <ToDoList />
      <Footer />
      <Switch>
        <Route path="/" exact={true} component={HomePage} />
        <Route path="/login" exact={true} component={LoginPage} />

      </Switch>
    </div>
  );
}

export default App;