export interface IAuthState() {
    isAuthenticated: boolean;
    message: string;
}