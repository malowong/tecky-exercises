import produce from 'immer'
import { IAuthAction } from './actions';

import { IAuthState } from "./state";

const initialState: IAuthState = {
    isAuthenticated: localStorage.getItem('token') !== null,
    msg: '',
}

export const authReducer = produce((state: IAuthState, action: IAuthAction) => {
    switch (action.type) {
        case "@@/Auth/LOGIN_SUCCESS":
            state.isAuthenticated = true
            return 
        case "@@/Auth/LOGIN_FAILED":
            state.isAuthenticated = false
            state.msg = action.msg
            return
    }
}, initialState)