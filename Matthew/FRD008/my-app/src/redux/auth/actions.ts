export function loginSuccess() {
    return {
        type: "@@/Auth/LOGIN_SUCCESS" as const,
    }
}

export function loginFailed() {
    return {
        type: "@@/Auth/LOGIN_FAILED" as const,
    }
}

export type IAuthAction = ReturnType<typeof loginSuccess> | ReturnType<typeof loginFailed>