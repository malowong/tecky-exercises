export function loginThunk(username, password) {
    return async (dispatch: Dispatch<any>) => {
        const resp = await fetchLogin(username, password)
        if (resp.status !== 200) {
            dispatch(failed("LOGIN_FAILED", result.msg))
        } else {
            localStorage.setItem("token", result.token)
            dispatch(loginSuccess)
            dispatch(push("/"))
        } 
    }
}


export function loginFacebook(accessToken:string){
    return async(dispatch:Dispatch<IAuthActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/login/facebook`,{
            method:'POST',
            headers:{
                "Content-Type":"application/json; charset=utf-8"
            },
            body: JSON.stringify({ accessToken})
        })
        const result = await res.json();

        if(res.status!==200){
            dispatch(failed("LOGIN_FAILED",result.msg));
        }else{
            localStorage.setItem('token',result.token);
            dispatch(loginSuccess())
            dispatch(push("/"));
        }
    }
}