import { Actions } from "./actions"

export interface Item {
    id: number,
    item: string,
    completed: boolean,
}


export interface IItemState {
    item: Item[],
}


const initialState: IItemState = {
    item: [],
    status: Status.All,
}

export function gameReducer(state: IItemState = initialState, action: Actions): IItemState {
    switch (action.type) {
        case '@@todos/ADD_ITEM':
            return {
                ...state,
                item: [...state.item, {
                    id: action.id,
                    name: action.name,
                    color: action.color,
                }]
            }
            // no need to add break as it has already returned.
    }
    return state
}