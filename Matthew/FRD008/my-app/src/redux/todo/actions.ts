export type Actions = ReturnType<typeof addItem> | ReturnType<typeof removeItem>

export function addItem(id: number, item: string){
    return {
        // you can have 'ADD_PLAYER' as a type for the type key
        type: '@@todos/ADD_ITEM' as const,
        id,
        item,
    }
}

export function removeItem(id: number, item: string){
    return {
        // you can have 'ADD_PLAYER' as a type for the type key
        type: '@@todos/REMOVE_ITEM' as const,
        id,
        item,
    }
}