import React from "react";

function LoginPage() {
  const fBOnCLick = () => {
    return null;
  };

  const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
    if (userInfo.accessToken) {
      dispatch(loginFacebook(userInfo.accessToken));
    }
    return null;
  };
  return (
    <div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <FormGroup>
          <Label for="username">Username</Label>
          <Input type="email" name="email" placeholder="Type in Username" innerRef={register} />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" name="password" placeholder="Type in Password" innerRef={register} />
        </FormGroup>
        {msg ? <Alert color="danger">{msg}</Alert> : ""}
        <Input type="submit" value="Submit" />
        <hr />
        <FormGroup>
          <div className="fb-button">
            <ReactFacebookLogin appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""} autoLoad={false} fields="name,email,picture" onClick={fBOnCLick} callback={fBCallback} />
          </div>
        </FormGroup>
      </Form>
    </div>
  );
}

export default LoginPage;
