import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import "./App.css";
import Blog from "./components/Blog";
import Bloglist from "./components/Bloglist";
import Game from "./components/Game";
import Homepage from "./components/Homepage";

// we have to use downgraded version of react-router-dom

function App() {
  
  return (
    <div className="App">
      <div className="link">
        <Link to="/">Homepage</Link>
        <Link to="/game">Game</Link>
        <Link to="/blogs">Blogs</Link>
      </div>
      <Switch>
        <Route path="/" exact><Homepage /></Route>
        <Route path="/game" exact><Game /></Route>   
        <Route path="/blogs" exact><Bloglist /></Route>
        <Route path="/blogs/:id" exact><Blog /></Route>
        <h1>Not Found 404</h1>
      </Switch>
    </div>
  );
}

export default App;