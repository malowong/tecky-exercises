import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { IRootState } from "../store";

export default function Blog() {

    // useParams help us to take the id from /blogs/:id
    const blogId = parseInt(useParams<{id: string}>().id)

    // we use useSelector here to find blog whom id is equal to its blogId.
    const blog = useSelector((state: IRootState) => {
        return state.blog.blogs.find(blog => blog.id === blogId)
    });

    return (
        <div>
            { blog == null ? <div>Blog not found</div> : (
                <div>
                    <h1>{blog.title}</h1>
                    <div>{blog.content}</div>
                </div>
            )}
            <Link to="/blogs">Back</Link>
        </div>
    )
}