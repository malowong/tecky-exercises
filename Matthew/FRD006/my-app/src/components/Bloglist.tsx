import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom";
import { IRootState } from "../store";

export default function Bloglist() {

    const blogs = useSelector((state: IRootState) => state.blog.blogs);


    return (
        <div>
            <h1>Blogs</h1>
            {blogs.map((blog) => (
                // for attribute value in backquote, we have to use a {}.
                <div key={blog.id}><Link to={`/blogs/${blog.id}`}>{blog.title}</Link></div>
            ))}
        </div>
    )
}