import { addLeaderboard } from "./actions"
import { leaderboardReducer } from "./reducers"

describe('leaderboard reducer', () => {
    it('should add leader', () => {
        const initialState = {
            leaders: []
        }

        const action = addLeaderboard("Matthew", "2020-01-02")

        const result = leaderboardReducer(initialState, action)

        expect(result).toEqual({
            leaders: [...initialState.leaders, {
                name: "Matthew",
                date: "2020-01-02",
            }]
        })
    })
})