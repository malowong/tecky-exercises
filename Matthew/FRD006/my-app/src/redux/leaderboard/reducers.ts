import { LeaderboardActions } from "./actions"

export interface ILeaderboardState {
    leaders: {
        name: string,
        date: string,
    }[]
}

const initialState: ILeaderboardState = {
    leaders: []
}

export function leaderboardReducer(state: ILeaderboardState = initialState, action: LeaderboardActions): ILeaderboardState {
    switch (action.type) {
        case '@@leaderboard/ADD_LEADERBOARD':
            return {
                leaders: [...state.leaders, {
                    name: action.name,
                    date: action.date,
                }] 
            }
    }
    return state
}