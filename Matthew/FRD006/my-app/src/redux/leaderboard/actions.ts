export type LeaderboardActions = ReturnType<typeof addLeaderboard> | ReturnType<typeof removeLeaderboard>

export function addLeaderboard(name: string, date: string){
    return {
        // you can have 'ADD_PLAYER' as a type for the type key
        type: '@@leaderboard/ADD_LEADERBOARD' as const,
        name,
        date,
    }
}

export function removeLeaderboard(name: string, date: string) {
    return {
        type: '@@leaderboard/REMOVE_PLAYER' as const,
        name,
        date,
    }
}