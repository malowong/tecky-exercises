export type GameActions = ReturnType<typeof addPlayer> | ReturnType<typeof playerMove>

export function addPlayer(id: number, name: string, color: string){
    return {
        // you can have 'ADD_PLAYER' as a type for the type key
        type: '@@game/ADD_PLAYER' as const,
        id,
        name,
        color
    }
}

export function playerMove(id: number){
    return {
        type: '@@game/PLAYER_MOVE' as const,
        id
    }
}