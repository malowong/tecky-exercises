import { GameActions } from "./actions"

export interface IGameState {
    players: Player[],
    progress: {
        // It's the new computed property name syntax. it is equal to:
        // let progess = {}
        // progress[playerId] = ""
        // so that you do not have to assign playerId in initialState. but have to add types.
        [playerId: string]: number,
    }
    losers: number[],
    canMove: boolean,
}

export interface Player {
    id: number,
    name: string,
    color: string,
}

const initialState: IGameState = {
    players: [],
    progress: {},
    losers: [],
    canMove: false,
}

export function gameReducer(state: IGameState = initialState, action: GameActions): IGameState {
    switch (action.type) {
        case '@@game/ADD_PLAYER':
            // we cannot push here. coz we have to respect immutability.
            return {
                ...state,
                players: [...state.players, {
                    id: action.id,
                    name: action.name,
                    color: action.color,
                }]
            }
            // no need to add break as it has already returned.
    }
    return state
}