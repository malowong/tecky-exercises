// since we do not have any action on blog (i.e. add blog / delete blog), we don't have to import actions.

export interface IBlogState {
    blogs: {
        id: number,
        title: string,
        content: string,
    }[]
}

const initialState: IBlogState = {
    blogs: [{
        id: 1,
        title: "this is the first blog",
        content: "this is the content of the first blog",
    }, {
        id: 2,
        title: "this is the second blog",
        content: "this is the content of the second blog",
    }],
}

export function blogReducer(state: IBlogState = initialState): IBlogState {
    return state;
}