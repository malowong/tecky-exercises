export interface IRootState {
    stopwatch: IStopwatchState,
    countdown: ICountdownState,
}

const reducer = combineReducers({
    stopwatch: stopwatchReducer,
})