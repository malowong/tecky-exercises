import { Knex } from "knex";
import { tables } from "../tables";

// get every increments an unique name! 

// in case you create your tables wrongly and want to change, you can either migrate one more time to alter
// in case you want to change the name of your table, you can  rollback then change the name of your table, then latest again

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tables.USER, (table) => {
        table.increments("user_id")
        table.string('username')
        table.string('password')
        table.string('level')
        table.timestamps(false,true)
    })

    await knex.schema.createTable(tables.CATEGORY, (table) => {
        table.increments("category_id")
        table.string('name')
        table.timestamps(false,true)
    })

    await knex.schema.createTable(tables.FILE, (table) => {
        table.increments("file_id")
        table.string('name')
        table.text('content')
        table.boolean('is_file').defaultTo(true)
        table.integer('category_id').unsigned()
        table.foreign('category_id').references(`${tables.CATEGORY}.category_id`)
        table.integer('user_id').unsigned()
        table.foreign('user_id').references(`${tables.USER}.user_id`)
        table.timestamps(false,true)
    })

}

// remember TENET: delete table in reverse order

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.FILE)
    await knex.schema.dropTable(tables.CATEGORY)
    await knex.schema.dropTable(tables.USER)
}