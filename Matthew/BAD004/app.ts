
import Knex from "knex"



async function main() {
    const knexConfig = require('./knexfile')
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

}

main()
