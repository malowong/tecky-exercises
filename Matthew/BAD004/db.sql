-- How many files does each user have?

-- my answer:
SELECT users.username, COUNT(files.user_id)
FROM users INNER JOIN files
ON files.user_id = users.user_id
GROUP BY users.username;

-- model answer:

-- why from files but not users? 
-- the logic is that we are counting from files and group by their user_id. but we do not have their name, so we join the users table.

-- the problem of using inner join is that: what if some user has 0 file? then he/she would not be showing on the result.
-- right join users is better.

-- also, if you are going to show the user_id as well, you have to insert user_id into groupby.

SELECT users.user_id, users.username, COUNT(files.file_id) AS count
FROM files
RIGHT JOIN users
ON files.user_id = users.user_id
GROUP BY users.user_id, users.username
ORDER BY count DESC;

-- How does the files distribute over different categories?

SELECT categories.name, COUNT(files.category_id)
FROM categories INNER JOIN files
ON files.category_id = categories.category_id
GROUP BY categories.name;

-- model answer:

SELECT files.category_id, categories.name, COUNT(files.category_id) AS count 
FROM files
RIGHT JOIN categories
ON files.category_id = categories.category_id
GROUP BY files.category_id, categories.name
ORDER BY count DESC;

-- How many files of the category important does alex have ?

SELECT COUNT(files.category_id)
FROM categories 
INNER JOIN files
ON files.category_id = categories.category_id
INNER JOIN users
ON files.user_id = users.user_id
WHERE files.category_id = 1
AND files.user_id = 2

-- How many users have more than 800 files ?

-- We learn to use HAVING and WITH here.
-- we have to use HAVING but not where to store the condition of the aggregation. and have to write out the whole COUNT.
-- WITH ... AS is for subquery. 

WITH tmp AS(
    SELECT users.user_id, users.username, COUNT(files.file_id) as count
    FROM users LEFT JOIN files
    ON users.user_id = files.user_id
    GROUP BY users.user_id, users.username
    HAVING COUNT(files.file_id) > 700
)
SELECT COUNT(*) FROM tmp;


