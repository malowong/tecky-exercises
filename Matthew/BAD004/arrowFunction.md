# Learn More about Arrow Functions

## Case 1:
```Javascript
const A = () => x, y, z
```
- x is the thing to return. it is the same as below:

```Javascript
const A = () => {
    return x, y, z
} 
```

## Case 2:
```Javascript
const B = () => {
    return x
} 
```

- remember to add the word return! 

## Case 3:
```Javascript
const C = () => ({
    object x
})
```

- we return the object x here. it is the same as below:

```Javascript
const C = () => {
    return {
        object x
    }
}
```

## Remarks:

- arrow function is suitable for using the keyword "this" as it will propagate to the parent scope.