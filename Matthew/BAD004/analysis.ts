import Knex from "knex"

async function main(){

    // knex config

    const knexConfig = require('./knexfile')
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

    const a = await knex.select('users.user_id', 'users.username')
        .count('files.file_id', {as: 'count'})
        .from('files')
        .rightJoin('users', 'files.user_id', 'users.user_id')
        .groupBy('users.user_id', 'users.username')
        .orderBy('count', 'desc')

    // SELECT users.user_id, users.username, COUNT(files.file_id) AS count
    // FROM files
    // RIGHT JOIN users
    // ON files.user_id = users.user_id
    // GROUP BY users.user_id, users.username
    // ORDER BY count DESC;

    console.log(a)

    const b = await knex.select('categories.category_id', 'categories.name')
        .count('files.category_id', {as: 'count'})
        .from('files')
        .rightJoin('categories', 'files.category_id', 'categories.category_id')
        .groupBy('categories.category_id', 'categories.name')
        .orderBy('count', 'desc')

    // SELECT files.category_id, categories.name, COUNT(files.category_id) AS count 
    // FROM files
    // RIGHT JOIN categories
    // ON files.category_id = categories.category_id
    // GROUP BY files.category_id, categories.name
    // ORDER BY count DESC;

    console.log(b)

    const c = await knex('categories')
        .count('files.category_id')
        .innerJoin('files', 'files.category_id', 'categories.category_id')
        .innerJoin('users', 'files.user_id', 'users.user_id')
        .where({
            'files.category_id': '1',
            'files.user_id': '2',
        })


    // SELECT COUNT(files.category_id)
    // FROM categories 
    // INNER JOIN files
    // ON files.category_id = categories.category_id
    // INNER JOIN users
    // ON files.user_id = users.user_id
    // WHERE files.category_id = 1
    // AND files.user_id = 2

    console.log(c)

    const d = await knex('tmp').with('tmp', (qb) => {
        qb.select('users.user_id', 'users.username')
        .count('files.file_id', {as: 'count'})
        .from('files')
        .rightJoin('users', 'files.user_id', 'users.user_id')
        .groupBy('users.user_id', 'users.username')
        .havingRaw('COUNT(files.file_id) > ?', [700])
    }).count('*')

    // WITH tmp AS(
    //     SELECT users.user_id, users.username, COUNT(files.file_id) as count
    //     FROM users LEFT JOIN files
    //     ON users.user_id = files.user_id
    //     GROUP BY users.user_id, users.username
    //     HAVING COUNT(files.file_id) > 700
    // )
    // SELECT COUNT(*) FROM tmp;

    console.log(d)

    const e = await knex('files').where('file_id', 1)

    console.log(e)

    await knex.destroy();

}

main()