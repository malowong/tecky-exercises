export const tables = Object.freeze({
    USER: "users",
    CATEGORY: "categories",
    FILE: "files",
})