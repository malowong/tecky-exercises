import Knex from "knex"
import xlsx from "xlsx"
import path from "path"
import { tables } from "./tables"

interface FileRow {
    name: string;
    Content?: string;
    is_file: number;
    category: string;
    owner: string;
}

const correctMap = Object.freeze({
    gordan: "gordon",
    alexs: "alex",
    admiin: "admin",
    ales: "alex",
    micheal: "michael",
});

async function main() {

    // knex config

    const knexConfig = require('./knexfile')
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

    // it is good to use transaction here. coz if anything go wrong we can stop the entire process by not committing the changes.
    // remember that all the knex is "TRX" under transaction. 

    const trx = await knex.transaction()

    try {
        // like seeding, you have to delete at first in order to get a correct result. we use truncate here. 
        // it is also okay to use knex(file).del(). but truncate is better coz you can do it in one knex line and reset its id. 
        // truncate it is also faster, as it will delete all without selecting. 

        await trx.raw(`TRUNCATE ${tables.USER}, ${tables.CATEGORY}, ${tables.FILE} RESTART IDENTITY CASCADE`);

        const filePath = path.join(__dirname, 'BAD004-exercise.xlsx')
        const workbook = xlsx.readFile(filePath)

        const userData = xlsx.utils.sheet_to_json(workbook.Sheets.user)
        const categoryData = xlsx.utils.sheet_to_json(workbook.Sheets.category)
        const fileData = xlsx.utils.sheet_to_json(workbook.Sheets.file)

        // we don't have to map the data coz the column name of the database is the same as the excel file.

        const insertedUsers = await trx(tables.USER).insert(userData).returning(["user_id", "username"])
        const insertedCategories = await trx(tables.CATEGORY).insert(categoryData).returning(["category_id", "name"])

        // BAD004 - Part 2 10:00
        // why don't we use for loop to select the cataegories from file then set the id? 
        // because it is not efficient. we should avoid looping database query.

        const userIdMap = insertedUsers.reduce((acc, cur) => {
            acc.set(cur.username, cur.user_id);
            return acc
        }, new Map())

        console.log(userIdMap);
        console.log(`[INFO] inserted user data`);

        const categoryIdMap = insertedCategories.reduce((acc, cur) => {
            acc.set(cur.name, cur.category_id)
            return acc
        }, new Map())

        console.log(categoryIdMap);
        console.log(`[INFO] inserted category data`);

        // you can also you for-of-loop to do the same thing: to create a idMap for inserting id into tables.FILE.

        // const userIdMap = new Map()
        // for (const insertedUser of insertedUsers){
        //     userIdMap.set(insertedUser.username, insertedUser.user_id)
        // }
        // const categoryIdMap = new Map()
        // for (const insertedCategory of insertedCategories){
        //     categoryIdMap.set(insertedCategory.name, insertedCategory.category_id)
        // }

        // so we found a problem here. that is some of the user_id in file database were null.
        // we use reduce to make a set to see what kinds of name are there in the fileData. turns out some of them were wrongly inputed.

        // QUESTION: why have {} not work?
        // if there is no {} after =>, it means that we will return everything after =>. so we have to add return after =>{}.

        const tmp = fileData.reduce((acc: any, cur: any) => {
            return acc.add(cur.owner)
        }, new Set())

        console.log(tmp)

        // to solve the problem we create a correctMap, so that we can get correct name for the wrong data.

        // QUESTION: how to concat the content such that the + would not exist? split("\n").join('') not working
        // we have to debug first. add a console.log(file) before return in the fileData.map, so that when it discover error,
        // it won't directly go to last. we can see the console.log result directly above the error.

        // we found that it is because we have null in content. as simple as that. add ?. will do.
 
        // batchInsert is for bulky file insertion. you will see your data is separated into multiple insertion.

        const insertedFiles = await trx.batchInsert(
            tables.FILE,
            fileData.map((file: FileRow) => {
                return { 
                    name: file.name,
                    content: file.Content?.split('\n').join(''),
                    is_file: file.is_file,
                    category_id: categoryIdMap.get(file.category),
                    user_id: userIdMap.get(file.owner) || userIdMap.get(correctMap[file.owner]),
                }
            })
        )
        console.log(insertedFiles)

        console.log(`[INFO] inserted file data`);

        await trx.commit()

    } catch (error) {
        console.error(error.message);
        await trx.rollback();
    } finally {
        // we have to destroy if we want knex to stop working. remember it's knex.destroy not trx.destroy.
        await knex.destroy();
    }

}

main()
