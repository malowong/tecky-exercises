import React, { useEffect, useRef, useState } from "react";
import styles from "../css/Stopwatch.module.css";

const INTERVAL = 64;

enum Mode {
  Start,
  Pause,
}

function timeDisplay(milliseconds: number) {
  const millisecondDisplay = (milliseconds % 1000).toString().padStart(3, "0");
  const second = Math.floor(milliseconds / 1000);
  const secondDisplay = (second % 60).toString().padStart(2, "0");
  const minuteDisplay = Math.floor(second / 60);
  return `${minuteDisplay}:${secondDisplay}:${millisecondDisplay}`;
}

export default function Stopwatch() {
  // useRef help us to store a value which will be persisted throughout the whole lifetime
  const refIntervalId = useRef<number>();
  const [milliseconds, setMilliseconds] = useState(0);
  const [mode, setMode] = useState<Mode>(Mode.Pause);
  const [laptimeArr, setLaptimeArr] = useState<number[]>([]);

  // if you did not click the pause button but countdown button, the setInterval will run continuously.
  // this useEffect is to ensure that the setInterval will stop when the component is unmounted.
  // but since we cannot take the intervalId directly from the startButtonHandler, we need to useRef to store the value.
  useEffect(() => {
    return () => {
      if (refIntervalId.current) {
        clearInterval(refIntervalId.current);
      }
    };
  }, []);

  const startButtonHandler = () => {
    // if there is no value
    if (mode === Mode.Pause) {
      // there is a need to assign the setInterval to a intervalId in order to clearInterval.
      // and since the type of current is not the same as intervalId, we need to add window.
      const intervalId = window.setInterval(() => {
        // remember to set milliseconds => milliseconds + 1000, but not miliseconds only
        setMilliseconds((milliseconds) => milliseconds + INTERVAL);
      }, INTERVAL);
      refIntervalId.current = intervalId;
      setMode(Mode.Start);
    } else if (mode === Mode.Start) {
      clearInterval(refIntervalId.current);
      setMode(Mode.Pause);
    }
  };

  const resetButtonHandler = () => {
    if (mode === Mode.Pause) {
      setMilliseconds(0);
      setLaptimeArr([]);
    } else if (mode === Mode.Start) {
      const current = milliseconds;
      const newLaptimeArr = laptimeArr.slice();
      newLaptimeArr.push(current);
      setLaptimeArr(newLaptimeArr);
    }
  };

  return (
    <div className={styles.stopwatch}>
      <h1>Stopwatch</h1>
      <h1>{timeDisplay(milliseconds)}</h1>
      <button onClick={startButtonHandler}>
        {
          // only expression is allowed. no for loop or if-else clause
          mode === Mode.Pause ? "Start" : "Pause"
        }
      </button>
      <button onClick={resetButtonHandler}>
        {mode === Mode.Pause ? "Reset" : "Lap"}
      </button>
      {laptimeArr.map((laptime, idx) => (
        <div key={idx}>
          <h1>
            Lap {idx + 1} : {timeDisplay(laptime - (laptimeArr[idx - 1] ?? 0))}
          </h1>
        </div>
      ))}
    </div>
  );
}
