import React, { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";
import styles from "../css/Countdown.module.css";

// it seems hard to make your own form in react. you can consider using the react-hook-form.

enum Mode {
  Start,
  Pause,
}

export default function Countdown() {
  const [seconds, setSeconds] = useState(0);
  const [counting, setCounting] = useState(0);
  const [mode, setMode] = useState<Mode>(Mode.Pause);
  const refIntervalId = useRef<number>()

  refIntervalId.current = undefined;

  const onSecondChange = (e: ChangeEvent<HTMLInputElement>) =>
    setSeconds(parseInt(e.target.value));

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setCounting(seconds);
    const intervalId = window.setInterval(() => {
      setCounting((counting) => counting - 0.064);
      refIntervalId.current = intervalId
      console.log("test")
    }, 64);
    setMode(Mode.Start);
  };

  useEffect(() => {
      return () => {
          if(refIntervalId.current){
              clearInterval(refIntervalId.current);
          }
      }
  }, [])

  const pauseButtonHandler = () => {
    if (mode === Mode.Start) {
        window.clearInterval(refIntervalId.current);
        setMode(Mode.Pause);
        setSeconds(counting);
    }
  };

  const resetButtonHandler = () => {
    setCounting(0)
    setMode(Mode.Pause);
  };

  return (
    <div className={styles.countdown}>
      <h1>Countdown</h1>

      <form onSubmit={onSubmit}>
        {/* you have to insert value and onchange here */}
        <div>
          <input type="text" onChange={onSecondChange} />
          seconds
        </div>
        <div>
          <input
            type={mode === Mode.Pause ? "submit" : "button"}
            value="Start"
          />
        </div>
      </form>
      <button onClick={pauseButtonHandler}>Pause</button>
      <button onClick={resetButtonHandler}>Reset</button>
      {/* put toFixed here. */}
      <h1>{counting.toFixed(2)}</h1>
    </div>
  );
}
