import React, { useState } from 'react';
import './App.css';
import './App.scss';
import Countdown from './components/Countdown';
import Stopwatch from './components/Stopwatch';


function App() {
  const [column, setColumn]= useState("stopwatch")

  return (
    <div className="App">
      <button onClick={() => setColumn("stopwatch")}>stopwatch</button>
      <button onClick={() => setColumn("countdown")}>countdown</button>
      
      {column === "stopwatch" && <Stopwatch />}
      {column === "countdown" && <Countdown />}
    </div>
  );
}

export default App;



