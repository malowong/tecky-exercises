import React from 'react';
import logo from './logo.png';
import './App.css';
import './App.scss';

const element = <h1>Simple Website</h1>


function App() {
  return (
    <div className="App">
      <header className="App-header">
        {element}
      </header>
      <section>
        <p>
        This is a simple website made without React. Try to convert this into React enabled.
        </p>
        <ol>
          <li>First, you need to use <span className="text">create-react-app </span> </li>
          <li>Second, you need to run <span className="text"> npm start</span> </li>
        </ol>
      </section>
      <footer className="App-footer">
        <img src={logo} className="App-logo" alt="logo" />
      </footer>
    </div>
  );
}

export default App;
